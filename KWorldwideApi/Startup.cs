using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using KWorldwide.Application.About;
using KWorldwide.Application.AutomapperProfile;
using KWorldwide.Application.Color;
using KWorldwide.Application.Country;
using KWorldwide.Application.Dealer;
using KWorldwide.Application.Distributor;
using KWorldwide.Application.Events;
using KWorldwide.Application.Products;
using KWorldwide.Application.Technology;
using KWorldwide.Business.About;
using KWorldwide.Business.Color;
using KWorldwide.Business.Country;
using KWorldwide.Business.Dealer;
using KWorldwide.Business.Distributor;
using KWorldwide.Business.Events;
using KWorldwide.Business.Products;
using KWorldwide.Business.Technology;
using KWorldwide.Common.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;

namespace KWorldwideApi
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddCors((options) =>
			{
				options.AddPolicy("AnyOrigin", (builder) =>
				{
					builder.AllowAnyOrigin().AllowAnyMethod();
				});
			});

			// requires using Microsoft.Extensions.Options
			services.Configure<DatabaseSettings>(Configuration.GetSection("DatabaseSettings"));
			
			services.AddControllers().AddNewtonsoftJson(options => {
				options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
			});

			services.AddAutoMapper(typeof(SharedAutoMapperProfile));
			services.AddAutoMapper(typeof(KWorldwideAutoMapperProfile));
			this.ConfigureCustomServices(services);

			//services.AddSwaggerGen(c =>
			//{
			//	c.SwaggerDoc("v1", new OpenApiInfo { Title = "KWorldwideApi", Version = "v1" });
			//});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthorization();

			app.UseCors((builder) =>
			{
				builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
			});

			// Enable middleware to serve generated Swagger as a JSON endpoint.
			//app.UseSwagger();

			// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
			// specifying the Swagger JSON endpoint.
			//app.UseSwaggerUI(c =>
			//{
			//	c.SwaggerEndpoint("/swagger/v1/swagger.json", "KWorldwideApi v1");
			//	c.RoutePrefix = string.Empty;
			//});

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}

		private void ConfigureCustomServices(IServiceCollection services)
		{

			var dbSettings = new DatabaseSettings()
			{
				ConnectionString = Configuration.GetSection("DatabaseSettings").GetSection("ConnectionString").Value,
				DatabaseName = Configuration.GetSection("DatabaseSettings").GetSection("DatabaseName").Value
			};

			services.AddSingleton<ICountryRepo>(provider => new CountryRepo(dbSettings));
			services.AddSingleton<ICountryService, CountryService>();

			services.AddSingleton<IAboutRepo>(provider => new AboutRepo(dbSettings));
			services.AddSingleton<IAboutService, AboutService>();

			services.AddSingleton<IEventRepo>(provider => new EventRepo(dbSettings));
			services.AddSingleton<IEventService, EventService>();

			services.AddSingleton<ITechnologyRepo>(provider => new TechnologyRepo(dbSettings));
			services.AddSingleton<ITechnologyService, TechnologyService>();

			services.AddSingleton<IDistributorRepo>(provider => new DistributorRepo(dbSettings));
			services.AddSingleton<IDistributorService, DistributorService>();

			services.AddSingleton<IDealerRepo>(provider => new DealerRepo(dbSettings));
			services.AddSingleton<IDealerService, DealerService>();

			services.AddSingleton<IColorRepo>(provider => new ColorRepo(dbSettings));
			services.AddSingleton<IColorService, ColorService>();

			services.AddSingleton<IProductRepo>(provider => new ProductRepo(dbSettings));
			services.AddSingleton<IProductService, ProductService>();

			services.AddSingleton<ISpecificationRepo>(provider => new SpecificationRepo(dbSettings));
			services.AddSingleton<ISpecificationService, SpecificationService>();
		}
	}
}
