﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KWorldwideApi.Models
{
	public class vmProductOtherInfo
	{
		public List<vmColorProductPhoto> ColorPhotos { get; set; }
		public List<vmFeaturedSpecifications> FeaturedSpecifications { get; set; }

		private vmProductOtherInfo()
		{
			ColorPhotos = new List<vmColorProductPhoto>();
			FeaturedSpecifications = new List<vmFeaturedSpecifications>();
		}

		public static vmProductOtherInfo CreateModel(List<vmColorProductPhoto> ColorPhotos, List<vmFeaturedSpecifications> FeatureSpecs)
		{
			return new vmProductOtherInfo()
			{
				ColorPhotos = ColorPhotos,
				FeaturedSpecifications = FeatureSpecs
			};
		}
	}
}
