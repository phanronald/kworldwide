﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KWorldwideApi.Models
{
	public class vmColorProductPhoto
	{
		public int ColorId { get; private set; }
		public string PhotoImageUrl { get; private set; }
		public string ColorImageUrl { get; private set; }
		public string ColorName { get; private set; }

		private vmColorProductPhoto()
		{
		}

		public static vmColorProductPhoto CreateModel(int colorId, string photoImgUrl, string colorImgUrl, string colorName)
		{
			return new vmColorProductPhoto()
			{
				ColorId = colorId,
				PhotoImageUrl = photoImgUrl,
				ColorImageUrl = colorImgUrl,
				ColorName = colorName
			};
		}
	}
}
