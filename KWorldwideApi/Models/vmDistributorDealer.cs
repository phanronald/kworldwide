﻿using KWorldwide.Application.Dealer.Model;
using KWorldwide.Application.Distributor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KWorldwideApi.Models
{
	public class vmDistributorDealer
	{
		public List<DistributorModel> AllDistributors { get; set; }
		public List<DealerModel> AllDealers { get; set; }

		private vmDistributorDealer()
		{
			AllDistributors = new List<DistributorModel>();
			AllDealers = new List<DealerModel>();
		}

		public static vmDistributorDealer CreateModel(List<DistributorModel> Distributrs, List<DealerModel> Dealers)
		{
			return new vmDistributorDealer()
			{
				AllDistributors = Distributrs,
				AllDealers = Dealers
			};
		}
	}
}
