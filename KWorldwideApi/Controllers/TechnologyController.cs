﻿using System.Threading.Tasks;
using KWorldwide.Application.Technology;
using Microsoft.AspNetCore.Mvc;

namespace KWorldwideApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TechnologyController : ControllerBase
	{
		private ITechnologyService _technologySvc;

		public TechnologyController(ITechnologyService technologySvc)
		{
			_technologySvc = technologySvc;
		}

		[HttpGet]
		[Route("get-all-technologies")]
		public async Task<IActionResult> GetAllTechnologies()
		{
			var technologies = await this._technologySvc.GetTechnologies();
			return new ObjectResult(technologies);
		}
	}
}
