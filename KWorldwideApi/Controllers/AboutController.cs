﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KWorldwide.Application.About;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KWorldwideApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AboutController : ControllerBase
	{
		private IAboutService _aboutSvc;
		public AboutController(IAboutService aboutService)
		{
			_aboutSvc = aboutService;
		}

		[HttpGet]
		[Route("get-about")]
		public async Task<IActionResult> GetAboutData()
		{
			var aboutData = await _aboutSvc.GetAllAbout();
			return new ObjectResult(aboutData);
		}
	}
}
