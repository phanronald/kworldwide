﻿using System.Threading.Tasks;
using KWorldwide.Application.Color;
using Microsoft.AspNetCore.Mvc;

namespace KWorldwideApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ColorController : ControllerBase
	{
		private IColorService _colorService;

		public ColorController(IColorService colorService)
		{
			_colorService = colorService;
		}

		[HttpGet]
		[Route("get-all-colors")]
		public async Task<IActionResult> GetAllColors()
		{
			var allColors = await this._colorService.GetAllColors();
			return new ObjectResult(allColors);
		}
	}
}
