﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KWorldwide.Application.Country;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KWorldwideApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class CountryController : ControllerBase
	{
		private ICountryService _countrySvc;

		public CountryController(ICountryService countryService)
		{
			_countrySvc = countryService;
		}

		[HttpGet]
		[Route("get-all-countries")]
		public async Task<IActionResult> GetAllCountries()
		{
			var countries = await this._countrySvc.GetAllCountries();
			return new ObjectResult(countries);
		}
	}
}
