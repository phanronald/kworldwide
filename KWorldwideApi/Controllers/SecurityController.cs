﻿using KWorldwideApi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace KWorldwideApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class SecurityController : ControllerBase
	{
		public SecurityController()
		{
			//https://www.programmersought.com/article/9242241361/
		}

		[HttpGet]
		[Route("decrypt-data")]
		public IActionResult DecryptData()
		{
			var key = "12345678900000001234567890000000";
			var AES_IV = "1234567890000000";
			var encryptedFinal = string.Empty;

			var data = new vmEncrypted()
			{
				EncryptedText = "0b17958fbc26e5b4676f2635a24b26673d4036831be40dd251d66c3ed4a73735" //"dcdbeaa28adfafda17acb8e2e6c09e2f"
			};

			byte[] inputBytes = HexStringToByteArray(data.EncryptedText);
			byte[] keyBytes = Encoding.UTF8.GetBytes(key.Substring(0, 32));
			using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
			{
				aesAlg.Key = keyBytes;
				aesAlg.IV = Encoding.UTF8.GetBytes(AES_IV.Substring(0, 16));

				ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
				using var msEncrypt = new MemoryStream(inputBytes);
				using CryptoStream csEncrypt = new CryptoStream(msEncrypt, decryptor, CryptoStreamMode.Read);
				using var srEncrypt = new StreamReader(csEncrypt);
				encryptedFinal = srEncrypt.ReadToEnd();
			}

			var b = encryptedFinal;

			return Ok();
		}

		public byte[] HexStringToByteArray(string s)
		{
			s = s.Replace(" ", "");
			byte[] buffer = new byte[s.Length / 2];
			for (int i = 0; i < s.Length; i += 2)
				buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
			return buffer;
		}

	}
}
