﻿using System.Threading.Tasks;
using KWorldwide.Application.Events;
using Microsoft.AspNetCore.Mvc;

namespace KWorldwideApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class EventsController : ControllerBase
	{
		private IEventService _eventSvc;

		public EventsController(IEventService eventService)
		{
			_eventSvc = eventService;
		}

		[HttpGet]
		[Route("get-event-by-event-type/{eventType}")]
		public async Task<IActionResult> GetEventByEventType(int eventType)
		{
			var eventByTypeData = await _eventSvc.GetEventsByEventType(eventType);
			return new ObjectResult(eventByTypeData);
		}

		[HttpGet]
		[Route("get-event-by-event-category-type/{eventType}/{eventCategoryType}")]
		public async Task<IActionResult> GetEventByEventCategoryType(int eventType, int eventCategoryType)
		{
			var eventByCategoryType = await _eventSvc.GetEventsByEventCategoryType(eventType, eventCategoryType);
			return new ObjectResult(eventByCategoryType);
		}
	}
}
