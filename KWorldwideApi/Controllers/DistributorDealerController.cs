﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KWorldwide.Application.Dealer;
using KWorldwide.Application.Distributor;
using KWorldwideApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KWorldwideApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class DistributorDealerController : ControllerBase
	{
		private IDistributorService _distributorSvc;
		private IDealerService _dealerSvc;

		public DistributorDealerController(IDistributorService distributorService,
			IDealerService dealerService)
		{
			_distributorSvc = distributorService;
			_dealerSvc = dealerService;
		}

		[HttpGet]
		[Route("get-distributors-dealers")]
		public async Task<IActionResult> GetDistributorsDealers()
		{
			var distributorsData = await _distributorSvc.GetAllDistributors();
			var dealersData = await _dealerSvc.GetAllDealers();
			var model = vmDistributorDealer.CreateModel(distributorsData.ToList(), dealersData.ToList());
			return new ObjectResult(model);
		}

		[HttpGet]
		[Route("get-distributor-by-id/{distributorId}")]
		public async Task<IActionResult> GetDistributorById(int distributorId)
		{
			var currentDistributor = await _distributorSvc.GetDistributorById(distributorId);
			return new ObjectResult(currentDistributor);
		}
	}
}
