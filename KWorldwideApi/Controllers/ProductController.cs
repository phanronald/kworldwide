﻿using System.Threading.Tasks;
using KWorldwide.Application.Color;
using KWorldwide.Application.Products;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using KWorldwideApi.Models;
using System.Collections.Generic;
using KWorldwide.Application.Products.Models;

namespace KWorldwideApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ProductController : ControllerBase
	{
		private IProductService _productService;
		private ISpecificationService _specificationSvc;
		private IColorService _colorService;

		public ProductController(IProductService productService,
			ISpecificationService specificationSvc,
			IColorService colorService)
		{
			_productService = productService;
			_specificationSvc = specificationSvc;
			_colorService = colorService;
		}

		[HttpGet]
		[Route("get-all-products")]
		public async Task<IActionResult> GetAllProducts()
		{
			var products = await this._productService.GetProducts();
			return new ObjectResult(products);
		}

		[HttpGet]
		[Route("get-product-info/{productId}")]
		public async Task<IActionResult> GetProductInfo(int productId)
		{
			var featuredProductSpecificationList = await _specificationSvc.GetAllProductSpecifications_ByProductId(productId);
			var featuredProductSpecification = featuredProductSpecificationList.Where(x => x.IsFeatured).ToList();

			var allSpecificiationIds = featuredProductSpecification.Select(x => x.SpecId);

			var featuredSpecificationList = await _specificationSvc.GetAllSpecifications();
			var featuredSpecification = featuredSpecificationList.Where(x => allSpecificiationIds.Contains(x.ProductSpecId));

			var featureSpecJoinProdSpec = from featureProdSpec in featuredProductSpecification
										  join featureSpec in featuredSpecification
										  on featureProdSpec.SpecId equals featureSpec.ProductSpecId
										  select vmFeaturedSpecifications.CreateModel(featureSpec.Name, featureProdSpec.SpecValue);

			var colorPhotoData = await _productService.GetProductColorPhotos_ById(productId);
			var allProdColorIds = colorPhotoData.Select(x => x.ColorId);

			var allColorIdsList = await _colorService.GetAllColors();
			var allColorIds = allColorIdsList.Where(x => allProdColorIds.Contains(x.ColorId));

			var colorJoinColorPhoto = from colorPhoto in colorPhotoData
									  join color in allColorIds
									  on colorPhoto.ColorId equals color.ColorId
									  select vmColorProductPhoto.CreateModel(color.ColorId, colorPhoto.ImageUrl, color.ImageUrl, color.ColorName);

			var model = vmProductOtherInfo.CreateModel(colorJoinColorPhoto.ToList(), featureSpecJoinProdSpec.ToList());
			return new ObjectResult(model);
		}

		[HttpGet]
		[Route("get-product-photos/{productId}")]
		public async Task<IActionResult> GetProductPhotos(int productId)
		{
			var productPhotos = await this._productService.GetProductPhotos_ByProductId(productId);
			return new ObjectResult(productPhotos);
		}

		[HttpGet]
		[Route("get-product-video/{productId}")]
		public async Task<IActionResult> GetProductVideo(int productId)
		{
			var productVideos = await this._productService.GetProductVideos_ByProductId(productId);
			return new ObjectResult(productVideos);
		}

		[HttpGet]
		[Route("get-product-specifications/{productId}")]
		public async Task<IActionResult> GetProductSpecifications(int productId)
		{
			var allProdSpecs = await _specificationSvc.GetAllProductSpecifications_ByProductId(productId);
			var allSpecIdsInProduct = allProdSpecs.Select(x => x.SpecId);

			var allSpecs = await _specificationSvc.GetAllSpecifications();
			var allSpecGroupIds = allSpecs
									.Where(x => allSpecIdsInProduct.Contains(x.ProductSpecId))
									.Select(x => x.SpecGroupId);

			var allSpecGroupsList = await _specificationSvc.GetAllSpecificationGroups();
			var allSpecGroups = allSpecGroupsList.Where(x => allSpecGroupIds.Contains(x.SpecGroupId));


			var listProductSpecGroupWithSpecs = new List<ProductSpecGroupWithSpecs>();
			allProdSpecs.ForEach((ProductSpecification productSpec) =>
			{
				var foundSpec = allSpecs.Where(x => x.ProductSpecId == productSpec.SpecId).FirstOrDefault();
				var foundSpecGroup = allSpecGroups.Where(x => x.SpecGroupId == foundSpec.SpecGroupId).FirstOrDefault();
				var tProdSpecGroupWithSpec = new ProductSpecGroupWithSpecs()
				{
					SpecGroupId = foundSpecGroup.SpecGroupId,
					SpecName = foundSpec.Name,
					SpecValue = productSpec.SpecValue
				};

				listProductSpecGroupWithSpecs.Add(tProdSpecGroupWithSpec);
			});

			listProductSpecGroupWithSpecs = listProductSpecGroupWithSpecs.GroupBy(x => x.SpecGroupId).SelectMany(y => y).ToList();
			var model = vmProductSpecification.CreateModel(listProductSpecGroupWithSpecs, allSpecGroups.ToList());
			return new ObjectResult(model);
		}
	}
}
