﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KWorldwide.Application.Products;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KWorldwideApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class CategoriesController : ControllerBase
	{
		private IProductService _productService;

		public CategoriesController(IProductService productService)
		{
			_productService = productService;
		}

		[HttpGet]
		[Route("get-all-categories")]
		public async Task<IActionResult> GetAllProductCategories()
		{
			var allCategories = await this._productService.GetProductCategories();
			return new ObjectResult(allCategories);
		}
	}
}
