﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KWorldwide.Application.Products;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KWorldwideApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class SubCategoriesController : ControllerBase
	{
		private IProductService _productService;

		public SubCategoriesController(IProductService productService)
		{
			_productService = productService;
		}

		[HttpGet]
		[Route("get-all-sub-categories")]
		public async Task<IActionResult> GetAllSubCategories()
		{
			var allSubCategories = await this._productService.GetProductSubCategories();
			return new ObjectResult(allSubCategories);
		}
	}
}
