﻿using KWorldwide.Common.Settings;
using KWorldwide.Data.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Distributor
{
	public class DistributorRepo : IDistributorRepo
	{
		private readonly IMongoCollection<DistributorModel> _distributorMongoCollection;
		private readonly string DistributorCollectionName = "Distributor";

		public DistributorRepo(IDatabaseSettings settings)
		{
			var client = new MongoClient(settings.ConnectionString);
			var database = client.GetDatabase(settings.DatabaseName);
			_distributorMongoCollection = database.GetCollection<DistributorModel>(DistributorCollectionName);
		}

		public async Task<List<DistributorModel>> GetAllDistributors()
		{
			if (_distributorMongoCollection != null && _distributorMongoCollection.AsQueryable().Any())
			{
				return await _distributorMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}

		public async Task<DistributorModel> GetDistributorById(int distributorId)
		{
			if (_distributorMongoCollection != null && _distributorMongoCollection.AsQueryable().Any())
			{
				var findDistroIdFilter = Builders<DistributorModel>.Filter.Eq(x => x.DistributorId, distributorId);
				return await _distributorMongoCollection.Find(findDistroIdFilter).SingleAsync();
			}

			return null;
		}
	}
}
