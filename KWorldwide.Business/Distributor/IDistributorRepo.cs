﻿using KWorldwide.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Distributor
{
	public interface IDistributorRepo
	{
		Task<List<DistributorModel>> GetAllDistributors();
		Task<DistributorModel> GetDistributorById(int distributorId);
	}
}
