﻿using KWorldwide.Common.Settings;
using KWorldwide.Data.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Dealer
{
	public class DealerRepo : IDealerRepo
	{
		private readonly IMongoCollection<DealerModel> _dealerMongoCollection;
		private readonly string DealerCollectionName = "Dealer";

		public DealerRepo(IDatabaseSettings settings)
		{
			var client = new MongoClient(settings.ConnectionString);
			var database = client.GetDatabase(settings.DatabaseName);
			_dealerMongoCollection = database.GetCollection<DealerModel>(DealerCollectionName);
		}

		public async Task<List<DealerModel>> GetAllDealers()
		{
			if (_dealerMongoCollection != null && _dealerMongoCollection.AsQueryable().Any())
			{
				return await _dealerMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}
	}
}
