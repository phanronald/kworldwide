﻿using KWorldwide.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KWorldwide.Business.Dealer
{
	public interface IDealerRepo
	{
		Task<List<DealerModel>> GetAllDealers();
	}
}
