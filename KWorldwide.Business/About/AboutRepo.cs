﻿using KWorldwide.Common.Settings;
using KWorldwide.Data.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.About
{
	public class AboutRepo : IAboutRepo
	{
		private readonly IMongoCollection<AboutModel> _aboutMongoCollection;
		private readonly string AboutCollectionName = "AboutPage";

		public AboutRepo(IDatabaseSettings settings)
		{
			var client = new MongoClient(settings.ConnectionString);
			var database = client.GetDatabase(settings.DatabaseName);
			_aboutMongoCollection = database.GetCollection<AboutModel>(AboutCollectionName);
		}

		public async Task<AboutModel> GetAboutData()
		{
			if (_aboutMongoCollection != null && _aboutMongoCollection.AsQueryable().Any())
			{
				return await _aboutMongoCollection.AsQueryable().FirstOrDefaultAsync();
			}

			return null;
		}
	}
}
