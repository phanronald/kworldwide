﻿using KWorldwide.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Country
{
	public interface ICountryRepo
	{
		Task<List<CountryModel>> GetAllCountries();
	}
}
