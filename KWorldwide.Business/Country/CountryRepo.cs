﻿using KWorldwide.Common.Settings;
using KWorldwide.Data.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Country
{
	public class CountryRepo : ICountryRepo
	{
		private readonly IMongoCollection<CountryModel> _countryMongoCollection;
		private readonly string CountryCollectionName = "Country";

		public CountryRepo(IDatabaseSettings settings)
		{
			var client = new MongoClient(settings.ConnectionString);
			var database = client.GetDatabase(settings.DatabaseName);
			_countryMongoCollection = database.GetCollection<CountryModel>(CountryCollectionName);
		}

		public async Task<List<CountryModel>> GetAllCountries()
		{
			if (_countryMongoCollection != null && _countryMongoCollection.AsQueryable().Any())
			{
				return await _countryMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}
	}
}
