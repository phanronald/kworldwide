﻿using KWorldwide.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Products
{
	public interface IProductRepo
	{
		Task<List<ProductCategoryModel>> GetProductCategories();
		Task<List<ProductColorPhotos>> GetAllProductColorPhotos();
		Task<List<ProductSubCategoryModel>> GetProductSubCategories();

		Task<List<ProductModel>> GetProducts();
		Task<List<ProductPhoto>> GetAllProductPhotos();
		Task<List<ProductVideo>> GetAllProductVideos();
	}
}
