﻿using KWorldwide.Common.Settings;
using KWorldwide.Data.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Products
{
	public class SpecificationRepo : ISpecificationRepo
	{
		private readonly IMongoCollection<SpecificationGroup> _specGroupMongoCollection;
		private readonly IMongoCollection<Specification> _specMongoCollection;
		private readonly IMongoCollection<ProductSpecification> _productSpecMongoCollection;

		private readonly string SpecGroupCollectionName = "SpecificationGroup";
		private readonly string SpecCollectionName = "Specification";
		private readonly string ProductSpecCollectionName = "ProductSpecifiation";

		public SpecificationRepo(IDatabaseSettings settings)
		{
			var client = new MongoClient(settings.ConnectionString);
			var database = client.GetDatabase(settings.DatabaseName);

			_specGroupMongoCollection = database.GetCollection<SpecificationGroup>(SpecGroupCollectionName);
			_specMongoCollection = database.GetCollection<Specification>(SpecCollectionName);
			_productSpecMongoCollection = database.GetCollection<ProductSpecification>(ProductSpecCollectionName);
		}

		public async Task<List<ProductSpecification>> GetAllProductSpecification()
		{
			if (_productSpecMongoCollection != null && _productSpecMongoCollection.AsQueryable().Any())
			{
				return await _productSpecMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}

		public async Task<List<SpecificationGroup>> GetAllSpecificationGroups()
		{
			if (_specGroupMongoCollection != null && _specGroupMongoCollection.AsQueryable().Any())
			{
				return await _specGroupMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}

		public async Task<List<Specification>> GetAllSpecifications()
		{
			if (_specMongoCollection != null && _specMongoCollection.AsQueryable().Any())
			{
				return await _specMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}
	}
}
