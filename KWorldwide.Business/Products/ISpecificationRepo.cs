﻿using KWorldwide.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Products
{
	public interface ISpecificationRepo
	{
		Task<List<SpecificationGroup>> GetAllSpecificationGroups();
		Task<List<Specification>> GetAllSpecifications();
		Task<List<ProductSpecification>> GetAllProductSpecification();
	}
}
