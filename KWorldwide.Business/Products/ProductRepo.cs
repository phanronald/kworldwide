﻿using KWorldwide.Common.Settings;
using KWorldwide.Data.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Products
{
	public class ProductRepo : IProductRepo
	{
		private readonly IMongoCollection<ProductCategoryModel> _productCategoryMongoCollection;
		private readonly IMongoCollection<ProductSubCategoryModel> _productSubCategoryMongoCollection;
		private readonly IMongoCollection<ProductModel> _productMongoCollection;
		private readonly IMongoCollection<ProductColorPhotos> _productColorPhotosMongoCollection;
		private readonly IMongoCollection<ProductPhoto> _productPhotoMongoCollection;
		private readonly IMongoCollection<ProductVideo> _productVideoMongoCollection;
		
		private readonly string ProductCategoryCollectionName = "ProductCategory";
		private readonly string ProductSubCategoryCollectionName = "ProductSubCategory";
		private readonly string ProductCollectionName = "Product";
		private readonly string ProductColorCollectionName = "ProductColorPhotos";
		private readonly string ProductPhotoCollectionName = "ProductPhotos";
		private readonly string ProductVideoCollectionName = "ProductVideos";
		
		public ProductRepo(IDatabaseSettings settings)
		{
			var client = new MongoClient(settings.ConnectionString);
			var database = client.GetDatabase(settings.DatabaseName);

			_productCategoryMongoCollection = database.GetCollection<ProductCategoryModel>(ProductCategoryCollectionName);
			_productSubCategoryMongoCollection = database.GetCollection<ProductSubCategoryModel>(ProductSubCategoryCollectionName);
			_productMongoCollection = database.GetCollection<ProductModel>(ProductCollectionName);

			_productColorPhotosMongoCollection = database.GetCollection<ProductColorPhotos>(ProductColorCollectionName);
			_productPhotoMongoCollection = database.GetCollection<ProductPhoto>(ProductPhotoCollectionName);
			_productVideoMongoCollection = database.GetCollection<ProductVideo>(ProductVideoCollectionName);
		}

		public async Task<List<ProductColorPhotos>> GetAllProductColorPhotos()
		{
			if (_productColorPhotosMongoCollection != null && _productColorPhotosMongoCollection.AsQueryable().Any())
			{
				return await _productColorPhotosMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}

		public async Task<List<ProductPhoto>> GetAllProductPhotos()
		{
			if (_productPhotoMongoCollection != null && _productPhotoMongoCollection.AsQueryable().Any())
			{
				return await _productPhotoMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}

		public async Task<List<ProductVideo>> GetAllProductVideos()
		{
			if (_productVideoMongoCollection != null && _productVideoMongoCollection.AsQueryable().Any())
			{
				return await _productVideoMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}

		public async Task<List<ProductCategoryModel>> GetProductCategories()
		{
			if (_productCategoryMongoCollection != null && _productCategoryMongoCollection.AsQueryable().Any())
			{
				return await _productCategoryMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}

		public async Task<List<ProductModel>> GetProducts()
		{
			if (_productMongoCollection != null && _productMongoCollection.AsQueryable().Any())
			{
				return await _productMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}

		public async Task<List<ProductSubCategoryModel>> GetProductSubCategories()
		{
			if (_productSubCategoryMongoCollection != null && _productSubCategoryMongoCollection.AsQueryable().Any())
			{
				return await _productSubCategoryMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}
	}
}
