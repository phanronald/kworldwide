﻿using KWorldwide.Common.Enums;
using KWorldwide.Common.Settings;
using KWorldwide.Data.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Events
{
	public class EventRepo : IEventRepo
	{
		private readonly IMongoCollection<EventModel> _eventMongoCollection;
		private readonly string EventCollectionName = "Events";

		public EventRepo(IDatabaseSettings settings)
		{
			var client = new MongoClient(settings.ConnectionString);
			var database = client.GetDatabase(settings.DatabaseName);
			_eventMongoCollection = database.GetCollection<EventModel>(EventCollectionName);
		}

		public async Task<List<EventModel>> GetAllEvents()
		{
			if (_eventMongoCollection != null && _eventMongoCollection.AsQueryable().Any())
			{
				return await _eventMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}
	}
}
