﻿using KWorldwide.Common.Enums;
using KWorldwide.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Events
{
	public interface IEventRepo
	{
		Task<List<EventModel>> GetAllEvents();
	}
}
