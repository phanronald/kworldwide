﻿using KWorldwide.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Technology
{
	public interface ITechnologyRepo
	{
		Task<List<TechnologyModel>> GetTechnologies();
	}
}
