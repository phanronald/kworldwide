﻿using KWorldwide.Common.Settings;
using KWorldwide.Data.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Technology
{
	public class TechnologyRepo : ITechnologyRepo
	{
		private readonly IMongoCollection<TechnologyModel> _technologyMongoCollection;
		private readonly string TechnologyCollectionName = "Technology";

		public TechnologyRepo(IDatabaseSettings settings)
		{
			var client = new MongoClient(settings.ConnectionString);
			var database = client.GetDatabase(settings.DatabaseName);
			_technologyMongoCollection = database.GetCollection<TechnologyModel>(TechnologyCollectionName);
		}

		public async Task<List<TechnologyModel>> GetTechnologies()
		{
			if (_technologyMongoCollection != null && _technologyMongoCollection.AsQueryable().Any())
			{
				return await _technologyMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}
	}
}
