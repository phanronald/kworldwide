﻿using KWorldwide.Common.Settings;
using KWorldwide.Data.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Color
{
	public class ColorRepo : IColorRepo
	{
		private readonly IMongoCollection<ColorModel> _colorMongoCollection;
		private readonly string ColorCollectionName = "Colors";

		public ColorRepo(IDatabaseSettings settings)
		{
			var client = new MongoClient(settings.ConnectionString);
			var database = client.GetDatabase(settings.DatabaseName);

			_colorMongoCollection = database.GetCollection<ColorModel>(ColorCollectionName);
		}

		public async Task<List<ColorModel>> GetAllColors()
		{
			if (_colorMongoCollection != null && _colorMongoCollection.AsQueryable().Any())
			{
				return await _colorMongoCollection.AsQueryable().ToListAsync();
			}

			return null;
		}
	}
}
