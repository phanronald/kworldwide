﻿using KWorldwide.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Business.Color
{
	public interface IColorRepo
	{
		Task<List<ColorModel>> GetAllColors();
	}
}
