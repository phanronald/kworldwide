import * as React from 'react';
import { Route, Switch } from 'react-router-dom';

import Layout from './components/layout/layout';

import HomeComponent from './components/home/home.component';
import CategoryListComponent from './components/category/list/category-listing.component';
import CategoryEditComponent from './components/category/edit/category-edit.component';

import ColorListComponent from './components/color/list/color-listing.component';
import ColorEditComponent from './components/color/edit/color-edit.component';

export const routes = <Layout><Switch>
	<Route exact path='/' component={HomeComponent} />
	<Route path='/category-list' component={CategoryListComponent} />
	<Route path='/category-edit/:categoryId' component={CategoryEditComponent} />

	<Route path='/color-list' component={ColorListComponent} />
	<Route path='/color-edit/:colorId' component={ColorEditComponent} />

</Switch></Layout>;
