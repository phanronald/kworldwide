import { IFetchResource } from './../models/shared/ifetchresource';

export class CommonService {
	
	public static fetchWithTimeout = async (url: string, options: IFetchResource):Promise<Response> => {

		const { timeout = 8000 } = options;
		const controller = new AbortController();
		const id = setTimeout(() => controller.abort(), timeout);
		const response = await fetch(url, {
			...options,
			signal: controller.signal
		});

		clearTimeout(id);
		return response;

	}

	public static GetUrlVars = (): any => {

        let queryStringDictionary: any = {};
        let hash: string[] = [];
        let allQueryStrings = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < allQueryStrings.length; i++) {

            hash = allQueryStrings[i].split('=');
            if(hash[1] === undefined) {
                continue;
            }
            
            queryStringDictionary[String(hash[0])] = hash[1];

        }
        
        return queryStringDictionary;
    }
    
    public static PreventBodyScroll = (isModal: boolean):void => {

		let htmlTag = document.getElementsByTagName('html')[0];
        htmlTag.style.overflow = (isModal ? 'hidden': 'auto');
	}

	public static GenerateGuid = (): string => {
		return [
			CommonService.GenerateRandomGuidParts(2), 
			CommonService.GenerateRandomGuidParts(1), 
			CommonService.GenerateRandomGuidParts(1),
			CommonService.GenerateRandomGuidParts(1), 
			CommonService.GenerateRandomGuidParts(3)
		].join("-");
	}

	private static GenerateRandomGuidParts = (count: number): string => {

		let out: string = "";
		for (let i: number = 0; i < count; i++) {
			out += (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
		}

		return out;
	}
}