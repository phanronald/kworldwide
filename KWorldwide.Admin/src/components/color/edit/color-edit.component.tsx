﻿import React, { Fragment, useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { IColor } from './../../../models/color/icolor';
import { IColorEditParams } from './../../../models/color/icoloreditparams';

const ColorEditComponent: React.FC = () => {

	const { colorId } = useParams<IColorEditParams>();

	const [colorName, setColorName] = useState<string>("");

	const history = useHistory();

	const useStyles = makeStyles({
		btnUpdateColor: {
			marginRight: '24px',
		},
	});

	const classes = useStyles();

	useEffect(() => {
		
		async function getColorByIdAsync() {

			const response = await fetch(process.env.API_URL + '/api/Colors/get-color-by-id/' + colorId);
			const foundColor: IColor = await response.json();
			setColorName(foundColor.ColorName);
		}

		getColorByIdAsync();

	}, []);

	const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, 
		property: string) => {

		const currentElement = e.target as HTMLInputElement;
		//(currentCategory as any)[property] = currentElement.value;
		switch(property)
		{
			case "ColorName": {
				setColorName(currentElement.value);
				break;
			}
			// case "SortOrder": {
			// 	setSortOrder(currentElement.value);
			// 	break;
			// }
		}
	};

	const updateColor = (event: React.MouseEvent<HTMLButtonElement>):void => {

		const formData = new FormData();
		formData.append('colorId', colorId);
		formData.append('colorName', colorName);

		// fetch(process.env.API_URL + '/api/Color/update-color', {
		// 	method: 'POST',
		// 	body: formData
		// })
		// .then(response => response.json())
		// .then(data => {
		// 	console.log(data);
		// })
		// .catch(error => {
		// 	console.error(error);
		// });

	}

	return (
  
	  <Fragment>

		<form>

			<div>
				<TextField id="colorNameId"
						label="Color Name"  helperText="Color Name"
						variant="filled" required
						value={colorName}
						onChange={ (event) => handleChange(event, "ColorName") } />
			</div>

			<div>
				<Button variant="contained" color="primary" className={classes.btnUpdateColor}
					onClick={(event) => updateColor(event) }>Update Color</Button>

				<Button variant="contained" color="secondary" 
					onClick={() => history.goBack() }>Cancel</Button>
			</div>

		</form>
		

		<span>{colorId}</span>

	  </Fragment>
  
	);
}
  
export default ColorEditComponent;