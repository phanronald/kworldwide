﻿import React, { Fragment, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { IColor } from './../../../models/color/IColor';

const ColorListComponent: React.FC = () => {

	const [allColors, setAllColors] = useState<IColor[]>([]);

	const useStyles = makeStyles({
		table: {
			minWidth: 650,
		},
	});

	const classes = useStyles();

	useEffect(() => {
		
	
		async function getColorsAsync() {

			const response = await fetch(process.env.API_URL + '/api/Colors/get-all-colors');
			const allColors: IColor[] = await response.json();
			setAllColors(allColors);

		}

		getColorsAsync();

	}, []);

	return (
  
	  <Fragment>

		<TableContainer component={Paper}>
			<Table className={classes.table} aria-label="simple table">
				<TableHead>
					<TableRow>
						<TableCell>Name</TableCell>
						<TableCell align="right">Image Url</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{
						allColors && allColors.length > 0 && 
						allColors.map((color: IColor) => (
							<TableRow key={color.Id}>
								<TableCell component="th" scope="row">							
									<Link to={"/color-edit/" + color.ColorId}> {color.ColorName} </Link>
								</TableCell>
								<TableCell align="right">
									<img src={color.ImageUrl} />
								</TableCell>
							</TableRow>
						))
					}
				</TableBody>
			</Table>
		</TableContainer>

	  </Fragment>
  
	);
}
  
export default ColorListComponent;