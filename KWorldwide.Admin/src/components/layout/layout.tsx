
import React, { useEffect,useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
import clsx from 'clsx';

import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';

const Layout: React.FC = (props) => {

	useEffect(() => {

	}, []);

	const drawerWidth = 240;

	const useStyles = makeStyles((theme: Theme) =>
		createStyles({
			root: {
				display: 'flex',
			},
			appBar: {
				transition: theme.transitions.create(['margin', 'width'], {
					easing: theme.transitions.easing.sharp,
					duration: theme.transitions.duration.leavingScreen,
				}),
			},
			appBarShift: {
				width: `calc(100% - ${drawerWidth}px)`,
				marginLeft: drawerWidth,
				transition: theme.transitions.create(['margin', 'width'], {
					easing: theme.transitions.easing.easeOut,
					duration: theme.transitions.duration.enteringScreen,
				}),
			},
			menuButton: {
				marginRight: theme.spacing(2),
			},
			hide: {
				display: 'none',
			},
			drawer: {
				width: drawerWidth,
				flexShrink: 0,
			},
			drawerPaper: {
				width: drawerWidth,
			},
			drawerHeader: {
				display: 'flex',
				alignItems: 'center',
				padding: theme.spacing(0, 1),
				// necessary for content to be below app bar
				...theme.mixins.toolbar,
				justifyContent: 'flex-end',
			},
			content: {
				flexGrow: 1,
				padding: theme.spacing(3),
				transition: theme.transitions.create('margin', {
					easing: theme.transitions.easing.sharp,
					duration: theme.transitions.duration.leavingScreen,
				}),
			},
			contentShift: {
				transition: theme.transitions.create('margin', {
					easing: theme.transitions.easing.easeOut,
					duration: theme.transitions.duration.enteringScreen,
				}),
				marginLeft: 0,
			},
		}),
	);

	const classes = useStyles();
	const theme = useTheme();
	const [open, setOpen] = useState(false);

	const handleDrawerOpen = () => {
		setOpen(true);
	};
	
	const handleDrawerClose = () => {
		setOpen(false);
	};

	return (

		<Fragment>
			
			<div className='main-container clearfix'>
				<div className='app-theme content-container'>
					<div className={classes.root}>
						<CssBaseline />
						<AppBar
							position="fixed"
							className={clsx(classes.appBar, {
							[classes.appBarShift]: open,
							})}
						>
							<Toolbar>
							<IconButton
								color="inherit"
								aria-label="open drawer"
								onClick={handleDrawerOpen}
								edge="start"
								className={clsx(classes.menuButton, open && classes.hide)}
							>
							<MenuIcon />
							</IconButton>
							<Typography variant="h6" noWrap>
								Electron Header
							</Typography>
							</Toolbar>
						</AppBar>
						<Drawer
							className={classes.drawer}
							variant="persistent"
							anchor="left"
							open={open}
							classes={{
								paper: classes.drawerPaper,
							}}
						>
							<div className={classes.drawerHeader}>
								<IconButton onClick={handleDrawerClose}>
									{theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
								</IconButton>
							</div>
							<Divider />
							<List>

								<ListItem button>
									<Link to="/" onClick={handleDrawerClose}> Home </Link>
								</ListItem>

								<ListItem button>
									<Link to="/category-list" onClick={handleDrawerClose}> Category </Link>
								</ListItem>

								<ListItem button>
									<Link to="/color-list" onClick={handleDrawerClose}> Color </Link>
								</ListItem>

							</List>
						</Drawer>
					</div>

					<main
						className={clsx(classes.content, {
						[classes.contentShift]: open,
					})}>

						<div className={classes.drawerHeader} />
						{props.children}

					</main>

				</div>
			</div>
			
		</Fragment>
	);
}

export default Layout;
