﻿import React, { Fragment, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { IProductCategory } from './../../../models/product/iproductcategory';

const CategoryListComponent: React.FC = () => {

	const [allCategories, setAllCategories] = useState<IProductCategory[]>([]);

	const useStyles = makeStyles({
		table: {
			minWidth: 650,
		},
	});

	const classes = useStyles();

	useEffect(() => {
		
	
		async function getProductCategoriesAsync() {

			const response = await fetch(process.env.API_URL + '/api/Categories/get-all-categories');
			const allCategories: IProductCategory[] = await response.json();
			setAllCategories(allCategories);

		}

		getProductCategoriesAsync();

	}, []);

	return (
  
	  <Fragment>

		<TableContainer component={Paper}>
			<Table className={classes.table} aria-label="simple table">
				<TableHead>
					<TableRow>
						<TableCell>Name</TableCell>
						<TableCell align="right">Image Url</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{
						allCategories && allCategories.length > 0 && 
						allCategories.map((category) => (
							<TableRow key={category.Id}>
								<TableCell component="th" scope="row">							
									<Link to={"/category-edit/" + category.ProductCategoryId}> {category.Name} </Link>
								</TableCell>
								<TableCell align="right">
									<img src={category.ImageUrl} />
								</TableCell>
							</TableRow>
						))
					}
				</TableBody>
			</Table>
		</TableContainer>

	  </Fragment>
  
	);
}
  
export default CategoryListComponent;