﻿import React, { Fragment, useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { IProductCategory } from './../../../models/product/iproductcategory';
import { ICategoryEditParams } from './../../../models/category/icategoryeditparams';

const CategoryEditComponent: React.FC = () => {

	const { categoryId } = useParams<ICategoryEditParams>();

	const [categoryName, setCategoryName] = useState<string>("");
	const [sortOrder, setSortOrder] = useState<string>("");

	const history = useHistory();

	const useStyles = makeStyles({
		btnUpdateCategory: {
			marginRight: '24px',
		},
	});

	const classes = useStyles();

	useEffect(() => {
		
		async function getProductCategoryByIdAsync() {

			const response = await fetch(process.env.API_URL + '/api/Categories/get-category-by-id/' + categoryId);
			const foundCategory: IProductCategory = await response.json();
			setCategoryName(foundCategory.Name);
			setSortOrder(foundCategory.SortOrder.toString());
		}

		getProductCategoryByIdAsync();

	}, []);

	const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, 
		property: string) => {

		const currentElement = e.target as HTMLInputElement;
		//(currentCategory as any)[property] = currentElement.value;
		switch(property)
		{
			case "Name": {
				setCategoryName(currentElement.value);
				break;
			}
			case "SortOrder": {
				setSortOrder(currentElement.value);
				break;
			}
		}
	};

	const updateCategory = (event: React.MouseEvent<HTMLButtonElement>):void => {

		const formData = new FormData();
		formData.append('productCategoryId', categoryId);
		formData.append('name', categoryName);
		formData.append('sortOrder', sortOrder);

		fetch(process.env.API_URL + '/api/Categories/update-category', {
			method: 'POST',
			body: formData
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
		})
		.catch(error => {
			console.error(error);
		});

	}

	return (
  
	  <Fragment>

		<form>

			<div>
				<TextField id="categoryNameId"
						label="Category Name"  helperText="Category Name"
						variant="filled" required
						value={categoryName}
						onChange={ (event) => handleChange(event, "Name") } />
			</div>

			<div>
				<TextField id="sortOrderId"
						label="Sort Order"  helperText="Sort Order"
						variant="filled" required
						value={sortOrder}
						onChange={ (event) => handleChange(event, "SortOrder") } />
			</div>

			<div>
				<Button variant="contained" color="primary" className={classes.btnUpdateCategory}
					onClick={(event) => updateCategory(event) }>Update Category</Button>

				<Button variant="contained" color="secondary" 
					onClick={() => history.goBack() }>Cancel</Button>
			</div>

		</form>
		

		<span>{categoryId}</span>

	  </Fragment>
  
	);
}
  
export default CategoryEditComponent;