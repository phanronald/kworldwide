﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Common.Settings
{
	public class DatabaseSettings : IDatabaseSettings
	{
		public string ConnectionString { get; set; }

		public string DatabaseName { get; set; }
	}
}
