﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Common.Settings
{
	public interface IDatabaseSettings
	{
		string ConnectionString { get; set; }
		string DatabaseName { get; set; }
	}
}
