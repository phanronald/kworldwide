﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Common.Enums
{
	public enum MediaType
	{
		None = 0,
		Image = 1,
		Video = 2
	}
}
