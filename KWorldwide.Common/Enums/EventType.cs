﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Common.Enums
{
	public enum EventType
	{
		None = 0,
		Regional = 1,
		Global = 2
	}
}
