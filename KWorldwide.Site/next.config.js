// next.config.js
module.exports = {

	basePath: '',
	onDemandEntries: {
		// period (in ms) where the server will keep pages in the buffer
		maxInactiveAge: 1000 * 60 * 60,
		// number of pages that should be kept simultaneously without being disposed
		pagesBufferLength: 5,
	},
	poweredByHeader: false,
	reactStrictMode: true,
	webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
		// Note: we provide webpack above so you should not `require` it
		// Perform customizations to webpack config
		//config.plugins.push(new webpack.IgnorePlugin(/\/__tests__\//))
	
		if(isServer) {
			require('./scripts/generate-sitemap');
		}

		// Important: return the modified config
		return config;
	},

	async headers() {
		return [
			{
				source: '/about',
					headers: [
					{
						key: 'Feature-Policy',
						// Disable microphone and geolocation
						value: "microphone 'none'; geolocation 'none'"
					},
					{
						key: 'Cache-Control',
						value: "max-age:604800, public"
					},
					{
						key: 'Strict-Transport-Security',
						value: "max-age:31536000; includeSubDomains; preload"
					},
					{
						key: 'x-xss-protection',
						value: '1; mode=block'
					},
					{
						key: 'x-frame-options',
						value: 'DENY'
					},
					{
						key: 'x-content-type-options',
						value: 'nosniff'
					}
				]
			}
		]
	},

	async redirects() {
		return [
			{
				source: '/aboutpage',
				destination: `${process.env.ABOUT_REDIRECT_PATH}`,
				permanent: true,
			}
		]
	}

}
  