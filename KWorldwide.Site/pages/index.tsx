
import React, { ChangeEvent } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { Carousel } from 'react-responsive-carousel';
import * as CryptoJS from 'crypto-js';

import styles from './../styles/carousel/carousel.module.css';

const Home: React.FC = () => {

	const uploadImage = (event: React.ChangeEvent<HTMLInputElement>):void => {

		const files = event.target.files;
		if(files && files.length > 0) {
			const formData = new FormData();
			formData.append('myFile', files[0]);
			formData.append('firstName', "Drizzt");
			formData.append('lastName', "Guider");

			fetch('http://localhost:8000/save-image', {
				method: 'POST',
				body: formData
			})
			.then(response => response.json())
			.then(data => {
				console.log(data);
			})
			.catch(error => {
				console.error(error);
			});
		}
	}

	const updateCategory = (event: React.MouseEvent<HTMLButtonElement>):void => {

		const formData = new FormData();
		formData.append('productCategoryId', '1');
		formData.append('name', "Motorcycle");
		formData.append('sortOrder', "1");

		fetch('http://localhost:8000/api/Categories/update-category', {
			method: 'POST',
			body: formData
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
		})
		.catch(error => {
			console.error(error);
		});

	}

	const addCategory = (event: React.MouseEvent<HTMLButtonElement>):void => {

		const formData = new FormData();
		formData.append('productCategoryId', '7');
		formData.append('name', "Drizzt");
		formData.append('sortOrder', "1");
		formData.append('navigationUrl', "/category/drizzt");
		formData.append('imageUrl', "/images/category/drizzt.png");

		fetch('http://localhost:8000/api/Categories/add-category', {
			method: 'POST',
			body: formData
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
		})
		.catch(error => {
			console.error(error);
		});

	}

	const deleteCategory = (event: React.MouseEvent<HTMLButtonElement>):void => {

		const formData = new FormData();
		formData.append('productCategoryId', '7');

		fetch('http://localhost:8000/api/Categories/delete-category', {
			method: 'POST',
			body: formData
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
		})
		.catch(error => {
			console.error(error);
		});

	}

	const encrtyptRandomTxt = (): void => {

		/*let KEY = "12345678900000001234567890000000";//32 bit
        let IV = "1234567890000000";//16 bits
        var key = CryptoJS.enc.Utf8.parse(KEY);
        var iv = CryptoJS.enc.Utf8.parse(IV);
 
        var srcs = CryptoJS.enc.Utf8.parse("I AM YAY");
        var encrypted = CryptoJS.AES.encrypt(srcs, key, {
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

		var data = {
			EncryptedText: encrypted.ciphertext.toString()
		};*/

		var data = [{id: 1}, {id: 2}]

		let KEY = "12345678900000001234567890000000";//32 bit
        let IV = "1234567890000000";//16 bits
        var key = CryptoJS.enc.Utf8.parse(KEY);
        var iv = CryptoJS.enc.Utf8.parse(IV);
 
        var encrypted = CryptoJS.AES.encrypt(JSON.stringify(data), key, {
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

		var dataToApi = {
			EncryptedText: encrypted.ciphertext.toString()
		};

		fetch('http://localhost:58891/api/security/decrypt-data', {
			method: 'POST',
			body: JSON.stringify(dataToApi)
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
		})
		.catch(error => {
			console.error(error);
		});
	}

	return (
  
	  <React.Fragment>
		<Head>
			<title>Index Page</title>
		</Head>
		<div>
			<Carousel showStatus={false} showThumbs={false} infiniteLoop={true}
				autoPlay={true} useKeyboardArrows={true} dynamicHeight={true}>
				<div>
					<img src="/images/home/kla-my19-zx10r.jpg" />
					<div className={styles.legend}>
						<div>
							<h2 className={styles['legend-heading-two']}>CHAMPIONSHIP-PROVEN POWER: </h2>
							<h3 className={styles['legend-heading-three']}>
								INTRODUCING THE NEW 2019 NINJA® ZX™-10R ABS KRT EDITION
							</h3>
						</div>
						
						<Link href='/products/2019-ninja-zx-10r-abs-krt-edition'>
							<a className={styles['legend-anchor']}>
								<span>SEE THE NEW 2019 NINJA® ZX™-10R ABS KRT</span>
							</a>
						</Link>
					</div>
				</div>
				<div>
					<img src="/images/home/kla_my19_mule_mx.jpg" />
					<div className={styles.legend}>
						<div>
							<h2 className={styles['legend-heading-two']}>CMID-SIZED AND FULLY CAPABLE: </h2>
							<h3 className={styles['legend-heading-three']}>
								INTRODUCING THE NEW MULE PRO-MX™ EPS LE
							</h3>
						</div>

						<Link href='/products/2019-mule-pro-mx-eps-le'>
							<a className={styles['legend-anchor']}>
								<span>SEE THE 2019 MULE PRO-MX™ EPS LE</span>
							</a>	
						</Link>
					</div>
				</div>
				<div>
					<img src="/images/home/kla_my19_teryx.jpg" />
					<div className={styles.legend}>
						<div>
							<h2 className={styles['legend-heading-two']}>PERFECT COMBINATION OF REAL STRENGTH AND UNMATCHED PERFORMANCE: </h2>
							<h3 className={styles['legend-heading-three']}>
								DOMINATE THE MOST DEMANDING TRAILS
							</h3>
						</div>

						<Link href='/products/2019-teryx4-800-fi-4x4-eps-le'>
							<a className={styles['legend-anchor']}>
								<span>SEE THE 2019 TERYX4™ 800 FI 4x4 EPS LE</span>
							</a>					
						</Link>
					</div>
				</div>
				<div>
					<img src="/images/home/kla_my19_brute-force.jpg" />
					<div className={styles.legend}>
						<div>
							<h2 className={styles['legend-heading-two']}>A CONQUEROR ON FOUR WHEELS: </h2>
							<h3 className={styles['legend-heading-three']}>
								SUPERIOR PERFORMANCE IN A VARIETY OF CONDITIONS
							</h3>
						</div>

						<Link href='/products/2019-brute-force-750-4x4i-eps'>
							<a className={styles['legend-anchor']}>
								<span>SEE THE 2019 BRUTE FORCE® 750 4x4i EPS</span>
							</a>
						</Link>
					</div>
				</div>
				<div>
					<img src="/images/home/kla_my19_jetski-sxr.jpg" />
					<div className={styles.legend}>
						<div>
							<h2 className={styles['legend-heading-two']}>Continuing the Legacy: </h2>
							<h3 className={styles['legend-heading-three']}>
								The New 2019 Stand-Up Jet Ski® SX-R™ Watercraft
							</h3>
						</div>

						<Link href='/products/2019-jet-ski-sx-r'>
							<a className={styles['legend-anchor']}>
								<span>SEE THE 2019 JET SKI® SX-R™</span>
							</a>	
						</Link>
					</div>
				</div>
			</Carousel>

			<input type="file" id="fileUpload" onChange={(event) => uploadImage(event) } />
			<button onClick={(event) => updateCategory(event) } >EDIT ONLY ONE CATEGORY</button>
			<button onClick={(event) => addCategory(event) } >ADD ONLY ONE CATEGORY</button>
			<button onClick={(event) => deleteCategory(event) } >DELETE ONLY ONE CATEGORY</button>
			<button onClick={(event) => encrtyptRandomTxt() } >ENCRYPT</button>
		</div>
	  </React.Fragment>
  
	);
  }
  
  export default Home;
  