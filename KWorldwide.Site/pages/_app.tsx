import React, { useEffect, Fragment, createContext } from 'react';
import App, { AppContext } from 'next/app'
import Head from 'next/head';
import { AppProps } from 'next/app';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from './../shared/theme/theme';

import { Consts } from './../config/consts';
import { IProductCategory } from './../models/product/iproductcategory';
import { CommonService } from './../services/common.service';

import Header from './../components/header/header';

import './../styles/global/flexboxgrid.min.css';
import './../styles/global/global.css';
import './../styles/global/global-component.css';
import './../styles/global/carousel.min.css';
import './../styles/global/header.css';
import './../styles/global/modal.css';

export default function MyApp(props: AppProps) {

	const { Component, pageProps } = props;
	const { categories } = pageProps;

	useEffect(() => {
		// Remove the server-side injected CSS.
		const jssStyles = document.querySelector('#jss-server-side');
		if (jssStyles) {
			jssStyles.parentElement!.removeChild(jssStyles);
		}

		if(CommonService.IsBrowser()) {
			CommonService.FetchAllCountries(true);
			CommonService.FetchAllProductCategories(true);
		}

	}, []);

	return (
		<Fragment>

			<Head>
				<title>KWorldwide Site</title>
				<meta charSet="utf-8" />
				<meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
				<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
				<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
				<script src="https://www.bing.com/api/maps/mapcontrol?key=AnekJEWOk3pvxuqcB-8csMuyZtKrZtCDidLxHPL7SHavR5h9_V4GwsL_UcfNHLzr"></script>
			</Head>
			<ThemeProvider theme={theme}>
				<CssBaseline />
				<div className="main-layout">
					<Header />
					<div className='content-container'>
						<Component {...pageProps} />
					</div>
				</div>
			</ThemeProvider>

		</Fragment>
	);
}

MyApp.getInitialProps = async (appContext: AppContext) => {

	if(process.env.NODE_ENV === 'development') {
		process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";
	}

	let apiUrl = process.env.API_URL;
	if(!apiUrl) {
		apiUrl = Consts.API_URL;
	}

	//const response = await fetch(apiUrl + '/api/Categories/get-all-categories');
	//const allCategories: IProductCategory[] = await response.json();
	const appProps = await App.getInitialProps(appContext);
	//appProps.pageProps.categories = allCategories;
	appProps.pageProps.categories = [];
	return { ...appProps }
	
}