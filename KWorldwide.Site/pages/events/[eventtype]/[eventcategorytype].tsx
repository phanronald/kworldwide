
import React, { useEffect, useState, Fragment } from 'react';
import { GetServerSideProps } from 'next';
import Link from 'next/link';
import Error from 'next/error';
import { useRouter } from 'next/router';
import Head from 'next/head';

import { Consts } from './../../../config/consts';
import { IEventProps } from './../../../models/events/props/ieventprops';
import { IEvent } from './../../../models/events/ievent';
import { CommonService } from './../../../services/common.service';
import { EventService } from './../../../services/event.service';
import { EventType } from '../../../models/shared/enums/eventtype';
import { EventCategoryType } from '../../../models/shared/enums/eventcategorytype';

import EventComponent from './../../../components/event/event.component';

const EventCategoryComponent: React.FC<IEventProps> = ({ eventsToShow }) => {

	const router = useRouter();
	const { eventtype, eventcategorytype } = router.query;
	const [isLoaded, setIsLoaded] = useState(false);
	const [newEventsToShow, setNewEventsToShow] = useState<IEvent[]>([] as IEvent[]);

	useEffect(() => {
		
		if(CommonService.IsServer()) {
			setNewEventsToShow(eventsToShow);
			setIsLoaded(true);
		}

		async function getAvailableCategoryViewModelToShow() {

			if(CommonService.IsBrowser()) {
				setIsLoaded(false);
				setUpEventCategoryTypes();
			}

		}

		getAvailableCategoryViewModelToShow();

	}, [eventcategorytype]);

	const setUpEventCategoryTypes = async (): Promise<void> => {

		let apiUrl = process.env.API_URL;
		if(!apiUrl) {
			apiUrl = Consts.API_URL;
		}
	
		let eventType: EventType = EventType.None;
		if(eventtype != undefined && eventtype != null && 
			typeof eventtype === 'string') {
	
			eventType = EventService.GetCurrentEventType(eventtype);
		}
	
		let eventCategoryType: EventCategoryType = EventCategoryType.None;
		if(eventcategorytype != undefined && eventcategorytype != null && 
			typeof eventcategorytype === 'string') {
	
				eventCategoryType = EventService.GetCurrentEventCategoryType(eventcategorytype);
		}
	
		const availableEventsByType = await EventService.FetchEventsByCategoryType(apiUrl, eventType, eventCategoryType);
		setNewEventsToShow(availableEventsByType);
		setIsLoaded(true);
	}

	return (
  
	  <React.Fragment>
			<Head>
				<title>Event Category Type Page</title>
			</Head>

			{
				isLoaded && newEventsToShow && newEventsToShow.length === 0 &&
				<Error statusCode={404} />
			}

			{
				isLoaded && newEventsToShow && newEventsToShow.length > 0 &&
				<EventComponent eventsToShow={newEventsToShow} countriesToShow={[]} />
			}

	  </React.Fragment>
  
	);
}
 
export const getServerSideProps: GetServerSideProps<IEventProps> = async ({ query, res }) => {

	if(process.env.NODE_ENV === 'development') {
		process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";
	}

	let apiUrl = process.env.API_URL;
	if(!apiUrl) {
		apiUrl = Consts.API_URL;
	}

	let eventType: EventType = EventType.None;
	if(query.eventtype != undefined && query.eventtype != null && 
		typeof query.eventtype === 'string') {

		eventType = EventService.GetCurrentEventType(query.eventtype);
	}

	let eventCategoryType: EventCategoryType = EventCategoryType.None;
	if(query.eventcategorytype != undefined && query.eventcategorytype != null && 
		typeof query.eventcategorytype === 'string') {

			eventCategoryType = EventService.GetCurrentEventCategoryType(query.eventcategorytype);
	}

	const availableEventsByType = await EventService.FetchEventsByCategoryType(apiUrl, eventType, eventCategoryType);
	if(availableEventsByType.length === 0 && res) {
		res.statusCode = 404;
	}

	return {
		props: {
			eventsToShow: availableEventsByType,
			countriesToShow: []
		}
	};

}
  

export default EventCategoryComponent;
  