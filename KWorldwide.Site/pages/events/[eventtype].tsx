
import React, { useEffect, useState, Fragment } from 'react';
import { GetServerSideProps } from 'next';
import Link from 'next/link';
import Error from 'next/error';
import { useRouter } from 'next/router';
import Head from 'next/head';

import { Consts } from './../../config/consts';
import { IEventProps } from './../../models/events/props/ieventprops';
import { IEvent } from './../../models/events/ievent';
import { CommonService } from './../../services/common.service';
import { EventService } from './../../services/event.service';
import { EventType } from '../../models/shared/enums/eventtype';

import EventComponent from './../../components/event/event.component';

const EventTypeComponent: React.FC<IEventProps> = ({ eventsToShow, countriesToShow }) => {

	const router = useRouter();
	const { eventtype } = router.query;
	const [isLoaded, setIsLoaded] = useState(false);
	const [newEventsToShow, setNewEventsToShow] = useState<IEvent[]>([] as IEvent[]);

	useEffect(() => {
		
		if(CommonService.IsServer()) {
			setNewEventsToShow(eventsToShow);
			setIsLoaded(true);
		}

		async function getAvailableCategoryViewModelToShow() {

			if(CommonService.IsBrowser()) {

				setNewEventsToShow(eventsToShow);
				setIsLoaded(true);
			}

		}

		getAvailableCategoryViewModelToShow();

	}, [eventtype]);

	return (
  
	  <React.Fragment>
			<Head>
				<title>Event Type Page</title>
			</Head>

			{
				isLoaded && newEventsToShow && newEventsToShow.length === 0 &&
				<Error statusCode={404} />
			}

			{
				isLoaded && newEventsToShow && newEventsToShow.length > 0 &&
				<EventComponent eventsToShow={newEventsToShow} countriesToShow={countriesToShow} />
			}
			

	  </React.Fragment>
  
	);
}

export const getServerSideProps: GetServerSideProps<IEventProps> = async ({ query, res }) => {

	if(process.env.NODE_ENV === 'development') {
		process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";
	}

	let apiUrl = process.env.API_URL;
	if(!apiUrl) {
		apiUrl = Consts.API_URL;
	}

	let allCountries = await CommonService.GetAllCountries();
	if(allCountries === null || allCountries.length === 0) {
		allCountries = await CommonService.FetchAllCountries(false);
	}

	let eventType: EventType = EventType.None;
	if(query.eventtype != undefined && query.eventtype != null && 
		typeof query.eventtype === 'string') {

		eventType = EventService.GetCurrentEventType(query.eventtype);
	}

	const availableEventsByType = await EventService.FetchEventsByType(apiUrl, eventType);
	if(availableEventsByType.length === 0 && res) {
		res.statusCode = 404;
	}
	
	return {
		props: {
			eventsToShow: availableEventsByType,
			countriesToShow: allCountries
		}
	};

}
  
export default EventTypeComponent;
  