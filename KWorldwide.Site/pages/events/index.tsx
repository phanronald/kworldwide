
import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';

import styles from './../../styles/events/events.module.css';

const EventsIndex: React.FC = () => {

	useEffect(() => {


	}, []);

	return (
  
		<React.Fragment>
			<Head>
				<title>Events Page</title>
			</Head>


			<div  className={styles['event-landing-container'] + " row"}>
				<div className={styles['event-header'] + " col-lg-9 col-md-11 col-sm-9 col-xs-11"}>
					<div className="col-lg col-md col-sm col-xs">
						<h1 className={styles['content-header-text']}>Worldwide Kawasaki Racing and Events</h1>
					</div>
				</div>
				<div className={styles['event-landing-body'] + " row"}>

					<div className="row">

						<div className="global-event-container col-lg col-md col-sm col-xs col-sm-offset-2 col-xs-offset-1">
							<Link href={"/events/[eventtype]"} as={'/events/global'}>
								<a>
									<img src="/images/events_landing_global_english.jpg" 
										className={styles['event-container-img']} alt="kawasaki Latin America" />
								</a>
							</Link>
						</div>
						<div className="regional-event-container col-lg col-md col-sm col-xs col-sm-offset-2 col-xs-offset-1">
							<Link href={"/events/[eventtype]"} as={'/events/regional'}>
								<a>
									<img src="/images/events_landing_region_english.jpg" 
										className={styles['event-container-img']} alt="kawasaki Latin America" />
								</a>
							</Link>
						</div>

					</div>

				</div>
			</div>

		</React.Fragment>
  
	);
}

export default EventsIndex;
  