
import React, { useEffect, useState, Fragment } from 'react';
import { GetServerSideProps } from 'next';
import Error from 'next/error';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Head from 'next/head';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Carousel } from 'react-responsive-carousel';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';

import Modal from './../../components/modal/modal';

import { Consts } from './../../config/consts';
import { Collection } from './../../shared/core/collection';
import { ITechnology } from './../../models/technology/itechnology';
import { ITechnologyProps } from './../../models/technology/props/itechnologyprops';
import { TechnologyType } from './../../models/shared/enums/technologytype';
import { MediaType } from './../../models/shared/enums/mediatype';
import { IMedia } from './../../models/technology/imedia';

import { CommonService } from './../../services/common.service';
import { ModalService } from './../../services/modal.service';
import { TechnologyService } from './../../services/technology.service';

import styles from './../../styles/technology/techname.module.css';

const TechnologyComponent: React.FC<ITechnologyProps> = ({ technologiesToShow }) => {

	const router = useRouter();
	const { techname } = router.query;

	const destroyStream: Subject<boolean> = new Subject<boolean>();

	const [isLoaded, setIsLoaded] = useState(false);
	const [newTechnologiesToShow, setNewTechnologiesToShow] = useState<ITechnology[]>([] as ITechnology[]);

	const [isEngineTech, setIsEngineTech] = useState(false);
	const [isChassisTech, setIsChassisTech] = useState(false);
	const [isTechnologyProvided, setIsTechnologyProvided] = useState(true);

	const [isModalOpen, setIsModalOpen] = useState(false);
	const [selectedTechnologyIndex, setSelectedTechnologyIndex] = useState(0);

	useEffect(() => {

		ModalService.openCloseModalEmitted
			.pipe(takeUntil(destroyStream))
			.subscribe((isOpen: boolean) => {
				
				setIsModalOpen(isOpen);

			});

		return function cleanup() {
			destroyStream.next(true);
			destroyStream.unsubscribe();
		};
	
	}, []);

	useEffect(() => {

		if(CommonService.IsServer()) {
			setNewTechnologiesToShow(technologiesToShow);
			setupTechnologies();
			setIsLoaded(true);		
		}

		async function getAvailableTechnologiesToShow() {

			if(CommonService.IsBrowser()) {

				let techType: TechnologyType = TechnologyType.None;
				if(techname != undefined && techname != null && 
					typeof techname === 'string') {
	
					techType = TechnologyService.GetCurrentTechnologyType(techname);
				}

				const avaiableTechnologies = await TechnologyService.FetchTechnologyByName(Consts.API_URL, techType);
				setNewTechnologiesToShow(avaiableTechnologies);
				setupTechnologies();
				setIsLoaded(true);
			}

		}

		getAvailableTechnologiesToShow();

	}, [techname]);

	const setupTechnologies = ():void => {

		if(techname && typeof techname === 'string')
		{
			switch(techname.toLowerCase())
			{
				case TechnologyType[TechnologyType.Engine].toLowerCase(): {
					setIsEngineTech(true);
					setIsChassisTech(false);
					setIsTechnologyProvided(true);
					break;
				}
				case TechnologyType[TechnologyType.Chassis].toLowerCase(): {
					setIsEngineTech(false);
					setIsChassisTech(true);
					setIsTechnologyProvided(true);
					break;
				}
				default: {
					setIsTechnologyProvided(false);
					break;
				}
			}
		}
		else {
			setIsTechnologyProvided(false);
		}
	}

	const seeMoreClickHandler = (techPosition: number) => (event: React.ChangeEvent<{}>) => {

		setIsModalOpen(true);
		setSelectedTechnologyIndex(techPosition);
	};

	const mediaClickHandler = (techMedia: IMedia) => (event: React.MouseEvent<HTMLElement>) => {

		const selectedThumbnailContainer: HTMLElement = ((event.target as HTMLElement)?.parentNode?.parentNode) as HTMLElement;
		for (let i = 0, len = selectedThumbnailContainer.children.length; i < len; i++) {
			let indivThumbnail = selectedThumbnailContainer.children[i] as HTMLElement;
			indivThumbnail.classList.remove('active');
		}

		(((event.target as HTMLElement).parentNode) as HTMLElement).classList.add('active');

		let contentMediaContainer: HTMLElement = ((selectedThumbnailContainer?.parentNode) as HTMLElement)?.querySelector('#contentMediaId') as HTMLElement;
		while (contentMediaContainer.firstChild) {
			contentMediaContainer.removeChild(contentMediaContainer.firstChild);
		}

		if (techMedia.Type === MediaType.Image) {
			let imageContainer: HTMLImageElement = document.createElement('img');
			imageContainer.src = techMedia.Image;
			contentMediaContainer.appendChild(imageContainer);
		}
		else {
			let videoContainer: HTMLVideoElement = document.createElement('video');
			videoContainer.src = techMedia.Video;
			videoContainer.controls = true;
			videoContainer.setAttribute('controlsList', 'nodownload');
			contentMediaContainer.appendChild(videoContainer);
		}
	}


	return (
  
		<React.Fragment>
			<Head>
			  <title>Per Technology Page</title>
			</Head>

			{
				isLoaded && !isTechnologyProvided &&
				<Error statusCode={404} />
			}

			<div className={styles['technology-container']}>
				<div className={styles['tech-listing-header'] + " row"}>
					{
						isLoaded && isTechnologyProvided && isEngineTech &&
						<div id="engineListingHeaderId" className={styles['tech-mgmt-container'] + " col-lg-11 col-md-11 col-sm-11 col-xs-11"}>
							<div className={styles['tech-mgmt-tech']}>
								<span>ENGINE MANAGEMENT TECHNOLOGY</span>
							</div>
							<div className={styles['tech-mgmt-txt']}>
								<span>Ever since manufacturing the first motorcycle engine in 1950,
									Kawasaki has been continually pursuing new technologies in the
									creation of our high-performance engines.</span>
							</div>
						</div>
					}

					{
						isLoaded && isTechnologyProvided && isChassisTech &&
						<div id="chassisListingHeaderId" className={styles['tech-mgmt-container'] + " col-lg-11 col-md-11 col-sm-11 col-xs-11"}>
							<div className={styles['tech-mgmt-tech']}>
								<span>CHASSIS MANAGEMENT TECHNOLOGY</span>
							</div>
							<div className={styles['tech-mgmt-txt']}>
								<span>The result of our advanced engineering, exhaustive testing and
								continuous pursuit of superior reliability, safety and performance.
								</span>
							</div>
						</div>
					}
				</div>
				<div className={styles['technology-listing-container'] + " row"}>
					{
						newTechnologiesToShow.map((technology: ITechnology, index: number) => {

							return (
								<div className={styles['technology-listing'] + " col-lg-4 col-md-4 col-sm-6 col-xs-12"} key={index} data-uid={technology.TechType}>
									<Card className="technology-item">
										<CardHeader className={styles['technology-header']} title={technology.Name}
											avatar={<img src={technology.Image} className={styles['technology-header-img']} />} />

										<CardContent>
											<div className={styles['technology-item-description']}
												dangerouslySetInnerHTML={{ __html: technology.TruncatedDescription }}>
											</div>
										</CardContent>
										<CardActions>
											<Button size="small" color="primary" className="event-seemore"
												onClick={seeMoreClickHandler(index+1)}>
												Read More
											</Button>
										</CardActions>
									</Card>
								</div>
							)
						})
					}
				</div>
			</div>

			{
				isModalOpen &&
				<Modal open={isModalOpen} className="modal-tech">
					<Carousel showStatus={true} showThumbs={false} infiniteLoop={true}
						autoPlay={false} useKeyboardArrows={true} dynamicHeight={true}
						showIndicators={false}>
							
						{/* <React.Fragment>
							{
								newTechnologiesToShow.map((technology: ITechnology, index: number) => {

									return (
										<div className="tech-slide" key={index}>
											<div className="feature-content">
												<div className="feature-content-header">
													<div className="feature-content-icon"></div>
													<div className="feature-content-title">
														{technology.Name}
													</div>
												</div>
												<div className="tech-feature-content-green-line"></div>
												<div className="feature-content-description">

													<div dangerouslySetInnerHTML={{ __html: technology.Description }}></div>

												</div>
											</div>
											<div className="media-content">
												<div className="media-display-container">
													{
														technology.Media.length > 0 &&
														<div id="contentMediaId">
															<img src={technology.Media[0].Image} />
														</div>
													}	
												</div>
												<div className="media-thumb-container">
													{
														technology.Media.map((techMedia: IMedia, index: number) => {
															return (
																<span className={'media-thumb-item' + (index === 0 ? ' active' : '')} key={index}
																	onClick={mediaClickHandler(techMedia)}>

																	<img src={techMedia.Image} />
																</span>
															)
														})
													}
												</div>
											</div>
										</div>
									)

								})
							}
						</React.Fragment> */}

					</Carousel>
				</Modal>
			}

		</React.Fragment>
  
	);
}

export const getServerSideProps: GetServerSideProps<ITechnologyProps> = async ({ query, res }) => {

	if(process.env.NODE_ENV === 'development') {
		process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";
	}

	let apiUrl = process.env.API_URL;
	if(!apiUrl) {
		apiUrl = Consts.API_URL;
	}

	let techType: TechnologyType = TechnologyType.None;
	if(query.techname != undefined && query.techname != null && 
		typeof query.techname === 'string') {

		techType = TechnologyService.GetCurrentTechnologyType(query.techname);
	}

	const avaiableTechnologies = await TechnologyService.FetchTechnologyByName(apiUrl, techType);
	if(avaiableTechnologies.length === 0 && res) {
		res.statusCode = 404;
	}

	return {
		props: {
			technologiesToShow: avaiableTechnologies
		}
	};

}

export default TechnologyComponent;
  