
import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import CustomLink from './../../components/shared/list-item-link';

import styles from './../../styles/technology/technology.module.css';

const TechnologyIndex: React.FC = () => {


	useEffect(() => {

	}, []);

	return (
  
		<React.Fragment>
			<Head>
				<title>Technology Page</title>
			</Head>

			<div className={styles['technology-landing']}>
				<img className={styles['technology-landing-image']} 
					src="/images/technology_landing_header.jpg" alt="Technology Page Header" />
					
				<div>

					<div className={styles['technology-container-header'] + " row"}>
						<h2 className={styles['tech-header-txt'] + " col-lg-8 col-md-8 col-sm-10 col-xs-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-1 col-xs-offset-1"}>
							&quot;Our mission is to enable riders to have optimum control of their
							high-performance Kawasaki motorcycle, so they may enjoy the pleasure of
							riding. Helping riders master the performance advantages offered by Kawasaki
							technology is our simple and innate desire.&quot;
						</h2>
					</div>
					<div className={styles['technology-body'] + " row"}>
						<div className={"technology-listing col-md-4 col-sm-5 col-md-offset-2 col-sm-offset-1"}>

							<Card className={styles['technology-card']}>
								<CardContent>
									<Typography component="h3" variant="h5" className={styles['technology-card-header']}>
										ENGINE MANAGEMENT TECHNOLOGY
									</Typography>
									<Typography component="p" className={styles['echnology-card-description']}>
										Ever since manufacturing the first motorcycle engine in 1950,
										Kawasaki has been continually pursuing new technologies in the creation of our
										high-performance engines.
									</Typography>
								</CardContent>
								<CardActions>
									<Button component={CustomLink} 
										href={"/technology/[techname]"} as={"/technology/engine"}
										size="small" color="primary">Learn More</Button>
								</CardActions>
							</Card>
						</div>

						<div className={"technology-listing col-md-4 col-sm-5"}>
							<Card className={styles['technology-card']}>
								<CardContent>
									<Typography component="h3" variant="h5" className={styles['technology-card-header']}>
										CHASSIS MANAGEMENT TECHNOLOGY
									</Typography>
									<Typography component="p" className={styles['echnology-card-description']}>
										The result of our advanced engineering, exhaustive testing and continuous
									pursuit of superior reliability, safety and performance.
								</Typography>
								</CardContent>
								<CardActions>
									<Button component={CustomLink}
										href={"/technology/[techname]"} as={"/technology/chassis"}
										size="small" color="primary">Learn More</Button>
								</CardActions>
							</Card>
						</div>
					</div>
				</div>
			</div>

		</React.Fragment>
  
	);
 }

 export default TechnologyIndex;
  