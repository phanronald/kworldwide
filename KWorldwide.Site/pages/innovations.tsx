import innovationdata from './../public/data/innovation.json';

import React, { useEffect, useState } from 'react';
import { GetStaticProps } from 'next';
import Head from 'next/head';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Carousel } from 'react-responsive-carousel';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';

import Modal from './../components/modal/modal';

import { Consts } from './../config/consts';
import { IInnovations } from './../models/innovations/iinnovations';
import { IInnovationsProp } from './../models/innovations/props/iinnovationsprop';

import { CommonService } from './../services/common.service';
import { ModalService } from './../services/modal.service';

import styles from './../styles/innovations/innovations.module.css';

const Innovations: React.FC<IInnovationsProp> = ({ fileContent }) => {

	const destroyStream: Subject<boolean> = new Subject<boolean>();

	const [innovationsToShow, setInnovationsToShow] = useState<IInnovations>({} as IInnovations);

	const [isVideoModalOpen, setIsVideoModalOpen] = useState(false);
	const [innovationDescription, setInnovationDescription] = useState('');
	const [slideNumber, setSlideNumber] = useState(0);
	const [innovationTitle, setInnovationTitle] = useState('');

	useEffect(() => {
		
		if(CommonService.IsServer()) {
			setInnovationsToShow(JSON.parse(fileContent));
		}

		async function getInitAvailableInnovations() {

			if(CommonService.IsBrowser()) {
				let innovationResponse = await fetch('/data/innovation.json');
				let innoviationObject: IInnovations = await innovationResponse.json();
				setInnovationsToShow(innoviationObject);
				setSlideNumber(0);
				setInnovationTitle(innoviationObject.Innovation[0].Title);
				setInnovationDescription(innoviationObject.Innovation[0].Description);
			}
		}

		getInitAvailableInnovations();

		ModalService.openCloseModalEmitted
			.pipe(takeUntil(destroyStream))
			.subscribe((isOpen: boolean) => {
				
				setIsVideoModalOpen(isOpen);

			});

		return function cleanup() {
			destroyStream.next(true);
			destroyStream.unsubscribe();
		};

	}, []);

	const carouselChangeEvent = (index:number, item: React.ReactNode): void => {
		const innovationToShow = innovationsToShow.Innovation[index];
		setSlideNumber(index);
		setInnovationTitle(innovationToShow.Title);
		setInnovationDescription(innovationToShow.Description);
	};

	const openVideoDialog = (event: React.MouseEvent<HTMLDivElement>) => {
		setIsVideoModalOpen(true);
	}

	return (
  
		<React.Fragment>
			<Head>
				<title>Innovations Page</title>
			</Head>
			<div className={styles['innovations']}>
				<div className={styles['innovations-row'] + " row"}>
					<div className={styles['inno-technology-listing'] +  " col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1 col-xs-6 col-xs-offset-1"}>
						<Card className={styles['innovations-item']}>
							<CardHeader className={styles['technology-info-header']} title="Kawasaki's World of Innovation" />
							<CardContent>
								<div className={styles['technology-information']}>
									<div className={styles['inno-heading']}>
										<span>{innovationTitle}</span>
									</div>
									<div className={styles['inno-copy']}>
									<span dangerouslySetInnerHTML={{ __html: innovationDescription }} />
										{
											slideNumber === 0 && 
											<div className={styles['inno-video-link'] + " inno-world-video-link"}
												onClick={openVideoDialog}>
													
												<span className={styles['inno-video-link-span']}>
													Kawasaki's World of Innovation
												</span>
											</div>
										}
									</div>
								</div>
							</CardContent>
							<CardActions>
								<Button size="small" color="primary" className={styles['innovation-heavy-industries']} 
									href="http://www.khi.co.jp/english/index.html ">

									Visit Kawasaki Heavy Industries
								</Button>
							</CardActions>
						</Card>
					</div>

					<div className={styles['inno-carousel-container'] + " col-xs-5 col-xs-offset-1"}>

						<Carousel showStatus={false} showThumbs={false} infiniteLoop={true}
							autoPlay={false} useKeyboardArrows={true} dynamicHeight={true}
							showIndicators={false} onChange={carouselChangeEvent}>
							<div>
								<img src="/images/khi/poster.jpg" />
							</div>
							<div>
								<img src="/images/khi/aviation.jpg" />
							</div>
							<div>
								<img src="/images/khi/bullet_train.jpg" />
							</div>
							<div>
								<img src="/images/khi/plants.jpg" />
							</div>
							<div>
								<img src="/images/khi/robotics.jpg" />
							</div>
							<div>
							<img src="/images/khi/motorcycle_engines.jpg" />
							</div>
						</Carousel>

					</div>
				</div>

				{
					isVideoModalOpen &&
					<Modal open={isVideoModalOpen}>
						<video controls autoPlay={true} controlsList="nodownload"
							src="http://www.kawasaki-la.com/cms/images/innovations/KHI_VIDEO_H264_640x360.MP4">
						</video>
					</Modal>
				}
			</div>
		</React.Fragment>
  
	);
}
  
export const getStaticProps: GetStaticProps<IInnovationsProp> = async () => {

	return {
		props: {
			fileContent: innovationdata as unknown as string
		}
	};

}

 export default Innovations;
  