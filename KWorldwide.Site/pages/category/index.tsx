
import React, { useEffect, useState } from 'react';
import { GetServerSideProps } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import { Consts } from './../../config/consts';
import { CommonService } from './../../services/common.service';
import { StorageUtility } from './../../shared/core/storage';

import { IProductCategoryIndexProps } from './../../models/product/props/iproductcategoryindexprops';
import { IProductCategory } from './../../models/product/iproductcategory';

const CategoryIndex: React.FC<IProductCategoryIndexProps> = ({ allCategories }) => {

	const [categories, setCategories] = useState<IProductCategory[]|null>([] as IProductCategory[]);

	const useStyles = makeStyles((theme: Theme) =>
		createStyles({
			root: {
			  flexGrow: 1,
			},
			paper: {
			  padding: theme.spacing(2),
			  textAlign: 'center',
			  color: theme.palette.text.secondary,
			  position: 'relative'
			},
			parentCategoryImageContainer: {
				padding: 8
			},
			categoryImage: {
				width: '100%'
			},
			categoryOverlay: {
				position: 'absolute',
				backgroundColor: 'rgba(0, 0, 0, 0.5)',
				left: 0,
				right: 0,
				bottom: 0,
				width: '100%',
				display: 'flex',
				justifyContent: 'center',	
			},
			categoryOverlayAnchor: {
				padding: '8px 0',
				width: '100%',
				color: '#fff',
				textDecoration: 'none'
			}
		}),
	);

	const classes = useStyles();

	useEffect(() => {

		if(CommonService.IsServer()) {
			setCategories(allCategories);
		}

		if(CommonService.IsBrowser()) {

			setCategories(StorageUtility.GetLocal(Consts.CATEGORY_KEY));
			if(categories === null) {
				CommonService.FetchAllProductCategories(true);
				setCategories(StorageUtility.GetLocal(Consts.CATEGORY_KEY));
			}
		}

	}, []);

	return (
  
	  <React.Fragment>
		<Head>
			<title>Category Page</title>
		</Head>
		
		<div className={classes.parentCategoryImageContainer}>
			<Grid container spacing={1}>

				{
					categories !== null && categories.map((category: IProductCategory, index: number) => {

						return (
							<Grid item xs={12} sm={6} key={category.Name.toLowerCase()}>
								<Paper className={classes.paper}>
									<img src={category.ImageUrl} alt={category.Name + " Image"}
										className={classes.categoryImage} />

									<div className={classes.categoryOverlay}>
										<Link href={"/category/[categoryname]"}
											as={'/category/' + category.Name.toLowerCase()}>
												
											<a className={classes.categoryOverlayAnchor}>
												{ category.Name.toUpperCase() }
											</a>
										</Link>
									</div>
								</Paper>
							</Grid>
						)
					})
				}

			</Grid>

		</div>

	  </React.Fragment>
  
	);
 }

export const getServerSideProps: GetServerSideProps<IProductCategoryIndexProps> = async () => {

	if(process.env.NODE_ENV === 'development') {
		process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";
	}

	let apiUrl = process.env.API_URL;
	if(!apiUrl) {
		apiUrl = Consts.API_URL;
	}
	
	const response = await fetch(apiUrl + '/api/Categories/get-all-categories');
	const allCategories: IProductCategory[] = await response.json();

	return {
		props: {
			allCategories: allCategories
		}
	};

}
  
 export default CategoryIndex;
  