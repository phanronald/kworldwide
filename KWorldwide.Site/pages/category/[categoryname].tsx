
import React, { useEffect, useState, Fragment } from 'react';
import { GetServerSideProps } from 'next';
import Link from 'next/link';
import Error from 'next/error';
import { useRouter } from 'next/router';
import Head from 'next/head';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import { Consts } from './../../config/consts';
import { Collection } from './../../shared/core/collection';
import { IProductSubCategory } from './../../models/product/iproductsubcategory';
import { IProduct } from './../../models/product/iproduct';
import { IProductCategoryProps } from './../../models/product/props/iproductcategoryprops';

import { CommonService } from './../../services/common.service';
import { ProductService } from './../../services/product.service';

import CategoryListComponent from './../../components/category/category-list.component';

const CategoryComponent: React.FC<IProductCategoryProps> = ({ 
	productsToShow, subCategoriesToShow 
}) => {

	const router = useRouter();
	const { categoryname } = router.query;

	const [newSubCategoriesShow, setNewSubCategoriesShow] = 
					useState<IProductSubCategory[]|null>([] as IProductSubCategory[]);

	const [newProductsToShow, setNewProductsToShow] = 
					useState<IProduct[]|null>([] as IProduct[]);

	const [ogProductsToShow, setOgProductsToShow] = 
					useState<IProduct[]|null>([] as IProduct[]);

	const [categorySelected, setCategorySelected] = useState(0);
	const [isLoaded, setIsLoaded] = useState(false);

	const useStyles = makeStyles((theme: Theme) =>
		createStyles({
			root: {
			  flexGrow: 1,
			},
			parentCategoryImageContainer: {
				padding: 8
			},
			paper: {
				padding: theme.spacing(2),
				textAlign: 'center',
				color: theme.palette.text.secondary,
				position: 'relative'
			  },
			categoryImage: {
				width: '100%'
			},
			categoryOverlay: {
				position: 'absolute',
				backgroundColor: 'rgba(0, 0, 0, 0.5)',
				left: 0,
				right: 0,
				bottom: 0,
				width: '100%',
				display: 'flex',
				justifyContent: 'center',	
			},
			categoryOverlayAnchor: {
				padding: '8px 0',
				width: '100%',
				color: '#fff',
				textDecoration: 'none'
			}
		}),
	);

	const classes = useStyles();

	useEffect(() => {
		
		async function getAvailableCategoryViewModelToShow() {

			if(CommonService.IsBrowser()) {

				setCategorySelected(0);
				let queryString: string = '';
				if(categoryname != undefined && categoryname != null && 
					typeof categoryname === 'string') {
	
					queryString = categoryname;
				}
	
				const vm = await ProductService.FetchCategoryViewModel(Consts.API_URL, queryString);
				setNewSubCategoriesShow(vm.subCategoriesToShow);
				setNewProductsToShow(vm.productsToShow);
				setOgProductsToShow(vm.productsToShow);
				setIsLoaded(true);
			}

		}

		getAvailableCategoryViewModelToShow();

	}, [categoryname]);

	const handleChange = (event: React.ChangeEvent<{}>, value: number) => {

		if (!Number.isNaN(value) && value !== 0) {

			setCategorySelected(value);
			if(ogProductsToShow != null) {
				setNewProductsToShow(new Collection(ogProductsToShow)
					.Where(x => x?.SubCategoryId === value).ToArray());
			}
		}
		else {

			setCategorySelected(0);
			if(ogProductsToShow != null) {
				setNewProductsToShow(ogProductsToShow);
			}
			
		}
	};

	return (
  
	  <React.Fragment>
			<Head>
				<title>Per Category Page</title>
			</Head>

			{
				CommonService.IsServer() &&
				<CategoryListComponent subCategoriesToShow={subCategoriesToShow} productsToShow={productsToShow} />
			}

			{
				isLoaded && newSubCategoriesShow?.length === 0 &&
				<Error statusCode={404} />
			}
			{
				isLoaded && newSubCategoriesShow && newSubCategoriesShow?.length > 0 &&
				<div className="i-am-client">
					<AppBar position="static" color="default" className="category-sub-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<Tabs
							value={categorySelected} onChange={ handleChange } indicatorColor="primary"
							textColor="primary" scrollButtons="on" variant="scrollable">

								<Tab label="All" value={0} />
								{
									newSubCategoriesShow !== null && newSubCategoriesShow.map((subCategory: IProductSubCategory, index: number) => {

										return (
											<Tab key={index} label={subCategory.Name} value={subCategory.ProductSubCategoryId} />
										)
									})
								}
							
						</Tabs>
					</AppBar>
					<div className={classes.parentCategoryImageContainer}>
						<Grid container spacing={1}>
							{
								newProductsToShow !== null && newProductsToShow.map((product: IProduct, index: number) => {

									return (
										<Grid item xs={12} sm={6} md={4} key={product.Name.toLowerCase()}>
											<Paper className={classes.paper}>
												<Link href={'/product/' + product.UrlKey}>
													<Fragment>
														<img alt={product.Name} src={product.NavImageUrl} />
														<div className="category-product-info">
															<span>{product.Year}</span>&nbsp;
															<span>{product.Name}</span>
														</div>
													</Fragment>
												</Link>
											</Paper>
										</Grid>
									)
								})
							}
						</Grid>
					</div>
					
				</div>
			}

	  </React.Fragment>
  
	);
}

export const getServerSideProps: GetServerSideProps<IProductCategoryProps> = async ({ query, res }) => {

	if(process.env.NODE_ENV === 'development') {
		process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";
	}

	let apiUrl = process.env.API_URL;
	if(!apiUrl) {
		apiUrl = Consts.API_URL;
	}

	let queryString: string = '';
	if(query.categoryname != undefined && query.categoryname != null && 
		typeof query.categoryname === 'string') {

		queryString = query.categoryname;
	}

	const vm = await ProductService.FetchCategoryViewModel(apiUrl, queryString);
	if(vm.subCategoriesToShow.length === 0 && vm.productsToShow.length === 0 && res) {
		res.statusCode = 404;
	}

	return {
		props: {
			productsToShow: vm.productsToShow,
			subCategoriesToShow: vm.subCategoriesToShow
		}
	};

}
  
export default CategoryComponent;
  