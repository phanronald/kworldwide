
import React, { useEffect, useState } from 'react';
import { GetServerSideProps } from 'next';
import Head from 'next/head';
import Link from 'next/link';

import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';
import Close from '@material-ui/icons/Close';

import { Consts } from './../../config/consts';
import { CommonService } from './../../services/common.service';
import { IDistributorLandingProp } from './../../models/distributor/props/idistributorlandingprop';
import { ICountry } from './../../models/country/icountry';
import { ILatLng } from './../../models/shared/models/maps/ilatlng';

import BingMaps from '../../components/maps/bing-maps';

import styles from './../../styles/distributor/distributor.landing.module.css';


const DistributorLanding: React.FC = () => {

	const [isLoaded, setIsLoaded] = useState(false);
	const [allCountriesToShow, setAllCountriesToShow] = useState<ICountry[]>([] as ICountry[]);
	const [selectedCountryString, setSelectedCountryString] = useState<string>("");

	const [mapCenter, setMapCenter] = useState<ILatLng>({} as ILatLng);

	useEffect(() => {
		
		if(CommonService.IsBrowser()) {
			setAllCountriesToShow(CommonService.GetAllCountries());
			setIsLoaded(true);
		}

		setMapCenter({
			lat: 34.0522,
			lng: 118.2437
		});

	}, []);

	return (
  
	  <React.Fragment>
		  <Head>
			  <title>Distributor Landing</title>
		  </Head>
		 
		 <div className={styles['distributors-container']}>
			<div className={styles['distributors-listing-container'] + " row"}>
				<div className={styles['distributors-listing'] + " col-lg-3 col-md-3 col-sm-4 col-xs-8"}>
					
					<div className={styles['distributor-title']}>
						<Typography component="h2" className={styles['distributor-title-header']}>
							Select a Country
						</Typography>

						<Typography component="p" className={styles['distributor-title-description']}>
							Please select a country to find your closest dealer’s contact information &amp; more.
						</Typography>
					</div>

					<ul className="distributor-countries-list">
						{
							allCountriesToShow.map((country: ICountry, index: number) => {

								const isSelected: boolean = selectedCountryString.toLowerCase() === country.UrlKey.toLowerCase() ? true : false;

								return (
									<li key={index} data-uid={country.CountryId}>
										<Link href={"/distributor/[distributorname]"} as={'/distributor/' + country.UrlKey}>

											<a data-urlkey={country.UrlKey} 
												className={country.UrlKey.toLowerCase() + " " + (isSelected ? "selected" : "")}>

												{country.Name}
											</a>

											
										</Link>
									</li>
								)

							})
						}

					</ul>

				</div>
				<div>
					{/* <BingMaps apiKey={(process.env.BING_MAP_URL as string)}
						latLng={mapCenter} title={"TITLE"} subtitle={"SUBTITLE"}
					/> */}
				</div>
			</div>
		 </div>

	  </React.Fragment>
  
	);
  }

  export default DistributorLanding;
  