
import React, { useEffect, useState } from 'react';
import { GetServerSideProps } from 'next';
import Head from 'next/head';
import useSWR from 'swr';

import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';
import Close from '@material-ui/icons/Close';

import { Consts } from './../../config/consts';
import { CommonService } from './../../services/common.service';
import { DistributorDealerService } from './../../services/distributor-dealer.service';
import { IDistributorComponentProp, IDistributorDealer } from './../../models/distributor/props/idistrubtorcomponentprops';
import { IDistributor } from './../../models/distributor/idistributor';
import { IDealer } from './../../models/distributor/idealer';
import styles from './../styles/distributor/distributor.landing.module.css';


const DistributorComponent: React.FC<IDistributorComponentProp> = ({ distributorDealerModel }) => {

	//const { data, error } = useSWR(Consts.API_URL + '/api/Distributors/get-all-distributors', fetch)
	//console.log(data, "YESSIR")

	useEffect(() => {
		
		//const { data, error } = useSWR(Consts.API_URL + '/api/Distributors/get-all-distributors', fetch)
		//console.log(data, "YESSIR")
		//const response = await fetch(Consts.API_URL + '/api/Categories/get-all-categories');
		/*const distributorDealerResponse = await fetch(apiUrl + '/api/DistributorDealer/get-all-distributors');
		const allDistributorDealers: IDistributorDealer = await distributorDealerResponse.json();
		return allDistributorDealers;
		
		let allCategories: IProductCategory[] = [];
		console.log(data, "YESSIR")
		if(data !== undefined) {
			allCategories = await data.json();

			if(doStoreLocalStorage) {
				StorageUtility.StoreLocal(Consts.CATEGORY_KEY, allCategories, Consts.EXPIRATION_OF_ONE_WEEK);
			}
		}

		//return allCategories;*/

	}, []);

	return (
  
	  <React.Fragment>
		  <Head>
			  <title>Distributor Landing</title>
		  </Head>

	  </React.Fragment>
  
	);
  }
  
  export const getServerSideProps: GetServerSideProps<IDistributorComponentProp> = async () => {

	if(process.env.NODE_ENV === 'development') {
		process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";
	}

	let apiUrl = process.env.API_URL;
	if(!apiUrl) {
		apiUrl = Consts.API_URL;
	}

	const landingProps: IDistributorDealer = await DistributorDealerService.FetchAllDistributorsDealers(apiUrl);

	return {
		props: {
			distributorDealerModel: landingProps
		}
	};

}

  export default DistributorComponent;
  