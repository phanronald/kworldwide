
import React, { useEffect, useState } from 'react';
import "bingmaps";

import { IBingMaps } from '../../models/shared/models/maps/ibingmaps';

declare var Microsoft: any;

const BingMaps: React.FC<IBingMaps> = (props: IBingMaps) => {
	
	let bingMap: Microsoft.Maps.Map;

	useEffect(() => {
		
		handleChangeMap();

	}, []);

	useEffect(() => {
		
		handleChangeMap();

	}, [props.latLng.lat, props.latLng.lng]);


	const handleChangeMap = (): void => {

		bingMap = new Microsoft.Maps.Map('#bingMapId',
		{
			center: new Microsoft.Maps.Location(props.latLng.lat, props.latLng.lng)
		});

		const center: Microsoft.Maps.Location = bingMap.getCenter();
		const centerPushPin = new Microsoft.Maps.Pushpin(center, {
			title: props.title,
			subTitle: props.subtitle,
			text: '1'
		});

		bingMap.entities.clear();
		bingMap.entities.push(centerPushPin);
	}

	return (
  
		<React.Fragment>

			<div id="bingMapId" className="bing-map"></div>

		</React.Fragment>
  
	);
  }
  
  export default BingMaps;
  