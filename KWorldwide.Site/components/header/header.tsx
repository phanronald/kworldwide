
import React, { useState, useEffect } from 'react';
import Link, { LinkProps } from 'next/link';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { IProductCategory } from './../../models/product/iproductcategory';
import { IHeader } from './../../models/shared/iheader';

import { Consts } from './../../config/consts';

import CustomLink from './../shared/list-item-link';

const Header: React.FC = () => {

	const useStyles = makeStyles((theme) => ({
		toolbar: {
			paddingRight: 24, // keep right padding when drawer closed
		},
		toolbarIcon: {
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'flex-end',
			padding: '0 8px',
			...theme.mixins.toolbar,
		},
		appBar: {
			zIndex: theme.zIndex.drawer + 1,
			transition: theme.transitions.create(['width', 'margin'], {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.leavingScreen,
			}),
		},
		appBarShift: {
			transition: theme.transitions.create(['width', 'margin'], {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.enteringScreen,
			}),
		},
		menuButton: {
			marginRight: 36,
		},
		menuButtonHidden: {
			display: 'none',
		},
		drawerPaper: {
			whiteSpace: 'nowrap',
			transition: theme.transitions.create('width', {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.enteringScreen,
			}),
		},
		drawerPaperClose: {
			overflowX: 'hidden',
			transition: theme.transitions.create('width', {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.leavingScreen,
			}),
			width: theme.spacing(7),
			[theme.breakpoints.up('sm')]: {
				width: theme.spacing(9),
			},
		}
	}));
	const classes = useStyles();

	const [openNavigation, setOpenNavigation] = useState(false);
	const [categories, setCategories] = useState<IProductCategory[]>([]);

	useEffect(() => {

		async function getInitCategories() {

			let apiUrl = process.env.API_URL;
			if(!apiUrl) {
				apiUrl = Consts.API_URL;
			}

			const response = await fetch(apiUrl + '/api/Categories/get-all-categories');
			const allCategories: IProductCategory[] = await response.json();
			setCategories(allCategories);
		}

		getInitCategories();

	}, []);


	const getCategoryUrl = (category: string): string => {
		const url: string = '/category/' + category.toLowerCase();
		return url;
	}

	const handleDrawerToggle = (shouldOpen: boolean) => (event: React.MouseEvent<HTMLElement>) => {
		setOpenNavigation(shouldOpen);
	};

	return (
  
	  <React.Fragment>

		<React.Fragment>
			<AppBar position="absolute" className={"app-bar " + (openNavigation ? "app-bar-shift" : "")}>
				<Toolbar disableGutters={!openNavigation}>
					<IconButton color="inherit" aria-label="open drawer"
						onClick={handleDrawerToggle(true)}
						className={clsx(classes.menuButton, openNavigation && classes.menuButtonHidden, 'app-menu-icon')}>
						<MenuIcon />
					</IconButton>
					<Link href="/">
						<a className="header-home-link">
							<Typography variant="h4" color="inherit" noWrap>
								Kawasaki
							</Typography>
						</a>
					</Link>
				</Toolbar>
			</AppBar>
			<SwipeableDrawer
				anchor="left"
				open={openNavigation}
				onClose={() => {}}
				onOpen={handleDrawerToggle(true)}
				classes={{
					paper: clsx(classes.drawerPaper, !openNavigation && classes.drawerPaperClose),
				}}
			>

				<div className="app-menu-toolbar">
					<IconButton onClick={handleDrawerToggle(false)}>
						<ChevronLeftIcon />
					</IconButton>
				</div>
				<Divider />
				<List>
					<div>
						{
							categories !== null && categories.map((category: IProductCategory, index: number) => {

								return (
									<ListItem button key={category.Name.toLowerCase()} 
										className="drawer-menu-list-item"
										component={CustomLink} 
										href={"/category/[categoryname]"}
										as={getCategoryUrl(category.Name)}
										onClick={handleDrawerToggle(false)}>

										<ListItemText primary={category.Name} />
									</ListItem>
								)
							})
						}
					</div>
				</List>
				<Divider />
				<List>
					<ListItem button className="drawer-menu-list-item" component={CustomLink} 
						href="/innovations"
						onClick={handleDrawerToggle(false)}>

						<ListItemText primary="Innovations" />
					</ListItem>
					<ListItem button className="drawer-menu-list-item" component={CustomLink} 
						href="/technology"
						onClick={handleDrawerToggle(false)}>

						<ListItemText primary="Technology" />
					</ListItem>
					<ListItem button className="drawer-menu-list-item" component={CustomLink} 
						href="/legal"
						onClick={handleDrawerToggle(false)}>

						<ListItemText primary="Legal/Privacy" />
					</ListItem>
					<ListItem button className="drawer-menu-list-item" 
						component={CustomLink} href="/about"
						onClick={handleDrawerToggle(false)}>
							
						<ListItemText primary="About" />
					</ListItem>
				</List>
			</SwipeableDrawer>
		</React.Fragment>

	  </React.Fragment>
  
	);
 }
  
 export default Header;
  