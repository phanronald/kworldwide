
import React, { useEffect, useState, lazy, Suspense, useRef } from 'react';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import { EventType } from './../../models/shared/enums/eventtype';
import { EventCategoryType } from './../../models/shared/enums/eventcategorytype';
import { IEvent } from './../../models/events/ievent';
import { ICountry } from './../../models/country/icountry';
import { IEventComponentProps } from './../../models/events/props/ieventcomponentprops';

import { CommonService } from './../../services/common.service';

import styles from './../../styles/events/event.component.module.css';

const EventComponent: React.FC<IEventComponentProps> = (props: IEventComponentProps) => {

	const originalEventsToShow = useRef(props.eventsToShow);
	const [eventsToShow, setEventsToShow] = useState<IEvent[]>([] as IEvent[]);
	const [countriesToShow, setCountriesToShow] = useState<ICountry[]>([] as ICountry[]);

	useEffect(() => {
		setEventsToShow(props.eventsToShow);
		setCountriesToShow(props.countriesToShow);
		
		let countrySelectedTriggeredEmitter = CommonService.EventCountrySelectedEmitter
		.subscribe('selectEventCountry', (countryIdSelected:number) => {

			
			let filteredCountryEvents = originalEventsToShow.current
									.filter(x => x.CountryId.includes(countryIdSelected));

			if(filteredCountryEvents.length > 0) {
				setEventsToShow(filteredCountryEvents);
			}
			
		});

		return function cleanup() {
			countrySelectedTriggeredEmitter.unsubscribe();
		};

	}, []);

	const EventMiniHeader = lazy(
		() => import('./event-mini-header.component')
	);

	return (

		<React.Fragment>
		 
			<div>
				{
					
					eventsToShow && eventsToShow.length > 0 && (
						<Suspense fallback={<div>Loading...</div>}>
							<EventMiniHeader currentEventType={EventType.Global} 
								currentEventCategoryType={EventCategoryType.Racing} />
						</Suspense>
					)
				}
				
			</div>

		 	<div className={styles['event-listing-container'] + " row"}>

				{
					eventsToShow && eventsToShow.length > 0 &&
					eventsToShow.map((event: IEvent, index: number) => {

						return (
							<div className={styles['event-listing'] + " col-lg-4 col-md-4 col-sm-6 col-xs-12"}
								key={index} data-uid={event.Id}>

								<Card className={styles['event-card']}>

									<CardMedia image={event.Image} title={event.Title} className={styles['event-image']} />
									<CardContent>
										<Typography variant="h1" component="div" className={styles['event-title']}>
											{event.Title}
										</Typography>
										<Typography component="div" className={styles['event-description']}>
											{event.Description}
										</Typography>
									</CardContent>
									<CardActions>
										<Button size="small" color="primary" className="event-seemore" href={event.Link}>
											Read More
										</Button>
									</CardActions>

								</Card>

							</div>
						)
					})
				}

			 </div>

		</React.Fragment>

	);
}
  
  export default EventComponent;
  