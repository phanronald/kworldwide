
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Link from 'next/link';
import AppBar from '@material-ui/core/AppBar';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';

import { IEventMiniHeaderProps } from './../../models/events/props/ieventminiheaderprops';
import { EventType } from './../../models/shared/enums/eventtype';
import { EventCategoryType } from './../../models/shared/enums/eventcategorytype';
import { CommonService } from './../../services/common.service';

const EventMiniHeader: React.FC<IEventMiniHeaderProps> = (props: IEventMiniHeaderProps) => {

	const [isGlobal, setIsGlobal] = useState(false);
	const [isGlobalRacing, setIsGlobalRacing] = useState(false);
	const [isGlobalConsumer, setIsGlobalConsumer] = useState(false);

	const [isRegional, setIsRegional] = useState(false);
	const [isRegionalRacing, setIsRegionalRacing] = useState(false);
	const [isRegionalConsumer, setIsRegionalConsumer] = useState(false);
	
	const [selectedCountryId, setSelectedCountryId] = useState(0);

	const useStyles = makeStyles((theme) => ({
		formControl: {
			margin: theme.spacing(1),
			minWidth: 120,
		}
	}));

	const classes = useStyles();

	useEffect(() => {
	
		const currrentEventType = props.currentEventType;
		const currentEventCategoryType = props.currentEventCategoryType;
		
		setIsGlobal(currrentEventType === EventType.Global && currentEventCategoryType === EventCategoryType.None);
		setIsGlobalRacing(currrentEventType === EventType.Global && currentEventCategoryType === EventCategoryType.Racing);
		setIsGlobalConsumer(currrentEventType === EventType.Global && currentEventCategoryType === EventCategoryType.Consumer);

		setIsRegional(currrentEventType === EventType.Regional && currentEventCategoryType === EventCategoryType.None);
		setIsRegionalRacing(currrentEventType === EventType.Regional && currentEventCategoryType === EventCategoryType.Racing);
		setIsRegionalConsumer(currrentEventType === EventType.Regional && currentEventCategoryType === EventCategoryType.Consumer);

	}, [props.currentEventType, props.currentEventCategoryType]);

	const selectCountryChange = (event: React.ChangeEvent<{ value: unknown }>) => {
		
		const currentSelectedCountryId = (event.target.value as number);
		setSelectedCountryId(currentSelectedCountryId);
		CommonService.EventCountrySelectedEmitter.emit('selectEventCountry', currentSelectedCountryId);

	};

	return (

		<React.Fragment>
		 
		 <AppBar position="static" color="default" className="event-sub-nav row">

			<div className="event-type-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<ul className="row event-type-listing">

					<li className="">
						<Link href={"/events/[eventtype]"} as='/events/global'>
							<a className={'event-type all-events ' + (isGlobal ? 'active' : '')}>
								All Global
							</a>			
						</Link>
					</li>

					<li className="">
						<Link href={"/events/[eventtype]/[eventcategorytype]"} as='/events/global/racing' >
							<a className={'event-type ' + (isGlobalRacing ? 'active' : '')}>
								Global Racing
							</a>			
						</Link>
					</li>

					<li className="">
						<Link href={"/events/[eventtype]/[eventcategorytype]"} as='/events/global/consumer'>
							<a className={'event-type ' + (isGlobalConsumer ? 'active' : '')}>
								Global Consumer
							</a>
						</Link>
					</li>

					<li className="">
						<Link href={"/events/[eventtype]"} as='/events/regional'>
							<a className={'event-type all-events ' + (isRegional ? 'active' : '')}>
								All Regional
							</a>
						</Link>
					</li>

					<li className="">
						<Link href={"/events/[eventtype]/[eventcategorytype]"} as='/events/regional/racing'>
							<a className={'event-type ' + (isRegionalRacing ? 'active' : '')}>
								Regional Racing
							</a>
						</Link>
					</li>

					<li className="">
						<Link href={"/events/[eventtype]/[eventcategorytype]"} as='/events/regional/consumer'>
							<a className={'event-type ' + (isRegionalConsumer ? 'active' : '')}>
								Regional Consumer
							</a>
						</Link>
					</li>

					{/* {
						(IsRegional || IsRegionalRacing || IsRegionalConsumer) &&
						<li className="country-dropdown-events">
							<Select className="select-menu-item" value={CountrySelected} onChange={this.handleCountryChange}>
								<MenuItem value={0}>Select Country</MenuItem>
								{
									this.state.CountriesToDisplay.map((country: ICountry, index: number) => {

										return (
											<MenuItem value={country.CountryId} key={index}>{country.Name}</MenuItem>
										)

									})
								}
							</Select>
						</li>
					} */}
				</ul>

			</div>
			<div>

				<FormControl className={classes.formControl}>
					<InputLabel id="eventSelectCountryId">Age</InputLabel>
					<Select
						labelId="Event Select Country"
						id="eventSelectCountryId"
						value={selectedCountryId}
						onChange={(event) => selectCountryChange(event)}
					>
						<MenuItem value={10}>Ten</MenuItem>
						<MenuItem value={20}>Twenty</MenuItem>
						<MenuItem value={30}>Thirty</MenuItem>
					</Select>
				</FormControl>

			</div>

		</AppBar>

		</React.Fragment>

	);
}
  
  export default EventMiniHeader;
  