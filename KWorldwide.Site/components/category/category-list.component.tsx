
import React, { useState, useEffect, Fragment } from 'react';
import Link from 'next/link';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import { IProductCategoryProps } from './../../models/product/props/iproductcategoryprops';
import { IProductSubCategory } from './../../models/product/iproductsubcategory';
import { IProduct } from './../../models/product/iproduct';

const CategoryListComponent: React.FC<IProductCategoryProps> = (props: IProductCategoryProps) => {

	const useStyles = makeStyles((theme: Theme) =>
		createStyles({
			root: {
			  flexGrow: 1,
			},
			parentCategoryImageContainer: {
				padding: 8
			},
			paper: {
				padding: theme.spacing(2),
				textAlign: 'center',
				color: theme.palette.text.secondary,
				position: 'relative'
			  },
			categoryImage: {
				width: '100%'
			},
			categoryOverlay: {
				position: 'absolute',
				backgroundColor: 'rgba(0, 0, 0, 0.5)',
				left: 0,
				right: 0,
				bottom: 0,
				width: '100%',
				display: 'flex',
				justifyContent: 'center',	
			},
			categoryOverlayAnchor: {
				padding: '8px 0',
				width: '100%',
				color: '#fff',
				textDecoration: 'none'
			}
		}),
	);

	const classes = useStyles();

	return (

		<React.Fragment>
		 
		 	<div className="i-am-error">
				<AppBar position="static" color="default" className="category-sub-nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<Tabs indicatorColor="primary"
						textColor="primary" scrollButtons="on" variant="scrollable">

							<Tab label="All" value={0} />
							{
								props.subCategoriesToShow !== null && props.subCategoriesToShow.map((subCategory: IProductSubCategory, index: number) => {

									return (
										<Tab key={index} label={subCategory.Name} value={subCategory.ProductSubCategoryId} />
									)
								})
							}
						
					</Tabs>
				</AppBar>
				<div className={classes.parentCategoryImageContainer}>
					<Grid container spacing={1}>
						{
							props.productsToShow !== null && props.productsToShow.map((product: IProduct, index: number) => {

								return (
									<Grid item xs={12} sm={6} md={4} key={product.Name.toLowerCase()}>
										<Paper className={classes.paper}>
											<Link href={'/product/' + product.UrlKey}>
												<Fragment>
													<img alt={product.Name} src={product.NavImageUrl} />
													<div className="category-product-info">
														<span>{product.Year}</span>&nbsp;
														<span>{product.Name}</span>
													</div>
												</Fragment>
											</Link>
										</Paper>
									</Grid>
								)
							})
						}
					</Grid>
				</div>
				
			</div>

		</React.Fragment>

	);
}
  
  export default CategoryListComponent;
  