
import React, { useEffect, useState } from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Close from '@material-ui/icons/Close';

import { IModal } from './../../models/shared/imodal';

import { ModalService } from './../../services/modal.service';

const Modal: React.FC<IModal> = (props: IModal) => {

	const [isModalOpen, setIsModalOpen] = useState(false);

	useEffect(() => {
		
		setIsModalOpen(props.open);

	}, [props.open]);

	const closeVideoDialog = (event: React.MouseEvent<SVGSVGElement>) => {
		ModalService.ApplyOpenCloseModalEmitted(false);
		setIsModalOpen(false);
	}

	return (
  
		<React.Fragment>

			<Dialog fullScreen={false} open={isModalOpen} maxWidth="md" className={'modal-container ' + props.className}>
				<DialogTitle className="modal-title">
					<Close className="modal-close-button" onClick={closeVideoDialog} />
				</DialogTitle>
				<DialogContent className="modal-dialog-conent">
					{props.children}
				</DialogContent>
				<DialogActions>

				</DialogActions>
			</Dialog>

		</React.Fragment>
  
	);
  }
  
  export default Modal;
  