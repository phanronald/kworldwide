﻿import { StorageModel } from "./../../models/shared/istorage";

export class StorageUtility<T> {

	constructor() {

	}

	public static GetLocal<T>(key: string): T | null {

		if(typeof window !== 'undefined' && typeof window.localStorage !== 'undefined') {
			const localStorageValue = localStorage.getItem(key);
			if (localStorageValue == null)
			{
				return null;
			}

			const result: StorageModel = JSON.parse((localStorageValue as string));
			const todayTime = new Date().getTime();
			if (todayTime >= result.timestamp)
			{
				return null;
			}

			return JSON.parse(result.value) as T;
		}

		return null;
	}

	public static StoreLocal<T>(key: string, item: T, expirationMin: number): void {

		if(typeof window !== 'undefined' && typeof window.localStorage !== 'undefined') {
			var expirationMS = expirationMin * 60 * 1000;
			var record: StorageModel = { value: JSON.stringify(item), timestamp: new Date().getTime() + expirationMS };
			localStorage.setItem(key, JSON.stringify(record));
		}

	}

	public static GetSession<T>(key: string): T | null {

		const sessionStorageValue = sessionStorage.getItem(key);
		if (sessionStorageValue == null) {
			return null;
		}

		const result: StorageModel = JSON.parse((sessionStorageValue as string));
		const todayTime = new Date().getTime();
		if (todayTime >= result.timestamp) {
			return null;
		}

		return JSON.parse(result.value) as T;
	}

	public static StoreSession<T>(key: string, item: T, expirationMin: number): void {

		var expirationMS = expirationMin * 60 * 1000;
		var record: StorageModel = { value: JSON.stringify(item), timestamp: new Date().getTime() + expirationMS };
		sessionStorage.setItem(key, JSON.stringify(record));

	}
}