﻿import { Enumerable } from "./../abstractions/enumerable";

export class Collection<T> extends Enumerable<T> {

	constructor(items: T[] = []) {
		super(items);
	}
}