﻿
interface IEvents { [event: string]: any[]; }

export interface IEventSubscription { unsubscribe: () => void; }

export class EventEmitter {
    public events: IEvents;
    constructor(events? : IEvents) {
        this.events = events || {};
    }

    public subscribe(name: string, cb: Function): IEventSubscription {
        (this.events[name] || (this.events[name] = [])).push(cb);
        return {
            unsubscribe: () => {
                this.events[name] &&
                    this.events[name].splice(this.events[name].indexOf(cb) >>> 0, 1)
            }
        };
    }

    public emit(name: string, ...args: any[]) {
        (this.events[name] || []).forEach(fn => fn(...args));
    }
}