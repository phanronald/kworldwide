
import { TechnologyType } from './../models/shared/enums/technologytype';
import { Collection } from './../shared/core/collection';
import { ITechnology } from './../models/technology/itechnology';

export class TechnologyService {

	public static FetchTechnologyByName = async (apiUrl: string, techType: TechnologyType): Promise<ITechnology[]> => {
	
		const technologyResponse = await fetch(apiUrl + '/api/Technology/get-all-technologies');
		const allTechnologies: ITechnology[] = await technologyResponse.json();
		const technologyCollection = new Collection(allTechnologies);
		technologyCollection.ForEach((tech: ITechnology | undefined, idx: number | undefined) => {
			if (tech !== undefined && tech !== null) {
				tech.TruncatedDescription = tech.Description.replace(/<[^>]+>/g, '').substring(0, 219) + '...';
			}
		});

		let finalFilteredArray: Array<ITechnology> = [];
		switch (techType) {
			case TechnologyType.Engine: {
				finalFilteredArray = technologyCollection.Where(x => x?.TechType === TechnologyType.Engine).ToArray();
				break;
			}
			case TechnologyType.Chassis: {
				finalFilteredArray = technologyCollection.Where(x => x?.TechType === TechnologyType.Chassis).ToArray();
				break;
			}
			default: {
				break;
			}

		}

		return finalFilteredArray;
		
	}

	public static GetCurrentTechnologyType = (techName: string): TechnologyType => {

		let currentTechType: TechnologyType = TechnologyType.None;
		switch(techName.toLowerCase())
		{
			case TechnologyType[TechnologyType.Engine].toLowerCase(): {
				currentTechType = TechnologyType.Engine;
				break;
			}
			case TechnologyType[TechnologyType.Chassis].toLowerCase(): {
				currentTechType = TechnologyType.Chassis;
				break;
			}
			default: {
				break;
			}
		}

		return currentTechType;
	}
}
