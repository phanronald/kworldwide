
import { Consts } from './../config/consts';
import { IProductCategory } from './../models/product/iproductcategory';
import { IProductSubCategory } from './../models/product/iproductsubcategory';
import { IProduct } from './../models/product/iproduct';

import { IProductCategoryProps } from './../models/product/props/iproductcategoryprops';

import { Collection } from './../shared/core/collection';

import { CommonService } from './common.service';

export class ProductService {

	public static FetchCategories = async () => {

	}

	public static FetchCategoryViewModel = async (apiUrl: string, categoryName: string): Promise<IProductCategoryProps> => {

		const allCategories: IProductCategory[] = await CommonService.FetchAllProductCategories(false);
		const categoryCollection = new Collection(allCategories);

		let foundCategoryId: number = 0;
		const foundCategory = categoryCollection.Where(x => x?.Name.toLowerCase() === categoryName.toLowerCase())
		if(foundCategory.Any()) {
			foundCategoryId = foundCategory.First().ProductCategoryId;
		}

		const productsToShow = await ProductService.FetchProductsByCategory(apiUrl, foundCategoryId);
		const subCategoriesToShow = await ProductService.FetchSubCategoriesByCategory(apiUrl, foundCategoryId);

		return {
			productsToShow: productsToShow,
			subCategoriesToShow: subCategoriesToShow
		} as IProductCategoryProps;	
	}

	public static FetchProductsByCategory = async (apiUrl: string, foundCategoryId: number): Promise<IProduct[]> => {
	
		const productResponse = await fetch(apiUrl + '/api/Product/get-all-products');
		const allProducts: IProduct[] = await productResponse.json();
		const productCollection = new Collection(allProducts);

		return productCollection.Where(x => x?.CategoryId === foundCategoryId).ToArray();
		
	}

	public static FetchSubCategoriesByCategory = async (apiUrl: string, foundCategoryId: number): Promise<IProductSubCategory[]> => {

		const subCategoryResponse = await fetch(apiUrl + '/api/SubCategories/get-all-sub-categories');
		const allSubCategories: IProductSubCategory[] = await subCategoryResponse.json();
		const subCategoryCollection = new Collection(allSubCategories);

		return subCategoryCollection.Where(x => x?.ProductCategoryId === foundCategoryId).ToArray();
	}
}
