import { Subject, Observable } from 'rxjs';

export class ModalService {

	protected static emitOpenCloseModal = new Subject<boolean>();
	public static openCloseModalEmitted = ModalService.emitOpenCloseModal.asObservable();

	public static ApplyOpenCloseModalEmitted = (isOpenModal:boolean): void => {
		ModalService.emitOpenCloseModal.next(isOpenModal);
	}
}
