
import { EventType } from './../models/shared/enums/eventtype';
import { EventCategoryType } from './../models/shared/enums/eventcategorytype';
import { Collection } from './../shared/core/collection';
import { IEvent } from './../models/events/ievent';

export class EventService {

	public static FetchEventsByType = async (apiUrl: string, eventType: number): Promise<IEvent[]> => {

		const eventsResponse = await fetch(apiUrl + '/api/Events/get-event-by-event-type/' + eventType.toString());
		const allEventsByType: IEvent[] = await eventsResponse.json();
		const eventsByTypeCollection = new Collection(allEventsByType);
		return eventsByTypeCollection.ToArray();
	}

	public static FetchEventsByCategoryType = async (apiUrl: string, eventType: number, eventCategoryType: number): Promise<IEvent[]> => {

		const eventsResponse = await fetch(apiUrl + '/api/Events/get-event-by-event-category-type/' + eventType.toString() + "/" + eventCategoryType.toString());
		const allEventsByCategoryType: IEvent[] = await eventsResponse.json();
		const eventsByCategoryTypeCollection = new Collection(allEventsByCategoryType);
		return eventsByCategoryTypeCollection.ToArray();
	}

	public static GetCurrentEventType = (eventType: string): EventType => {

		let currentEventType: EventType = EventType.None;
		switch(eventType.toLowerCase())
		{
			case EventType[EventType.Global].toLowerCase(): {
				currentEventType = EventType.Global;
				break;EventType
			}
			case EventType[EventType.Regional].toLowerCase(): {
				currentEventType = EventType.Regional;
				break;
			}
			default: {
				break;
			}
		}

		return currentEventType;
	}

	public static GetCurrentEventCategoryType = (eventCategoryType: string): EventCategoryType => {

		let currentEventCategoryType: EventCategoryType = EventCategoryType.None;
		switch(eventCategoryType.toLowerCase())
		{
			case EventCategoryType[EventCategoryType.Racing].toLowerCase(): {
				currentEventCategoryType = EventCategoryType.Racing;
				break;EventType
			}
			case EventCategoryType[EventCategoryType.Consumer].toLowerCase(): {
				currentEventCategoryType = EventCategoryType.Consumer;
				break;
			}
			default: {
				break;
			}
		}

		return currentEventCategoryType;
	}
}
