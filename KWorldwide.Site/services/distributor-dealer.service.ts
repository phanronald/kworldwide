
import { Collection } from './../shared/core/collection';
import { IDistributorDealer } from './../models/distributor/props/idistrubtorcomponentprops';
import { IDistributor } from './../models/distributor/idistributor';
import { IDealer } from './../models/distributor/idealer';

export class DistributorDealerService {

	public static FetchAllDistributorsDealers = async (apiUrl: string): Promise<IDistributorDealer> => {
	
		const distributorDealerResponse = await fetch(apiUrl + '/api/Distributors/get-all-distributors');
		const allDistributorDealers: IDistributorDealer = await distributorDealerResponse.json();
		return allDistributorDealers;
	}
}
