
import useSSR from 'use-ssr';
import { Consts } from './../config/consts';
import { ICountry } from './../models/country/icountry';
import { IProductCategory } from './../models/product/iproductcategory';
import { StorageUtility } from './../shared/core/storage';

import { EventEmitter } from './../shared/eventemitter';
export class CommonService {

	public static EventCountrySelectedEmitter = new EventEmitter();

	public static IsBrowser = (): boolean => {

		const { isBrowser } = useSSR();
		return isBrowser;
	};

	public static IsServer = (): boolean => {
		const { isServer } = useSSR();
		return isServer;
	};
	
	public static GetUrlVars = (): any => {

        let queryStringDictionary: any = {};
        let hash: string[] = [];
        let allQueryStrings = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < allQueryStrings.length; i++) {

            hash = allQueryStrings[i].split('=');
            if(hash[1] === undefined) {
                continue;
            }
            
            queryStringDictionary[String(hash[0])] = hash[1];

        }
        
        return queryStringDictionary;
    }
    
    public static PreventBodyScroll = (isModal: boolean):void => {

		let htmlTag = document.getElementsByTagName('html')[0];
        htmlTag.style.overflow = (isModal ? 'hidden': 'auto');
	}

	public static GenerateGuid = (): string => {
		return [
			CommonService.GenerateRandomGuidParts(2), 
			CommonService.GenerateRandomGuidParts(1), 
			CommonService.GenerateRandomGuidParts(1),
			CommonService.GenerateRandomGuidParts(1), 
			CommonService.GenerateRandomGuidParts(3)
		].join("-");
	}

	public static FetchAllCountries = async (doStoreLocalStorage: boolean): Promise<ICountry[]> => {
		const response = await fetch(Consts.API_URL + '/api/Country/get-all-countries');
		const allCountries: ICountry[] = await response.json();

		if(doStoreLocalStorage) {
			StorageUtility.StoreLocal(Consts.COUNTRY_KEY, allCountries, Consts.EXPIRATION_OF_ONE_WEEK);
		}

		return allCountries;
	}
	
	public static FetchAllProductCategories = async (doStoreLocalStorage: boolean): Promise<IProductCategory[]> => {
		const response = await fetch(Consts.API_URL + '/api/Categories/get-all-categories');
		const allCategories: IProductCategory[] = await response.json();

		if(doStoreLocalStorage) {
			StorageUtility.StoreLocal(Consts.CATEGORY_KEY, allCategories, Consts.EXPIRATION_OF_ONE_WEEK);
		}

		return allCategories;
	}

	public static GetAllCountries = ():ICountry[] => {
		const countriesInStorage = StorageUtility.GetLocal<ICountry[]>(Consts.COUNTRY_KEY);
		if(countriesInStorage != null) {
			return countriesInStorage;
		}

		return [];
	}

	private static GenerateRandomGuidParts = (count: number): string => {

		let out: string = "";
		for (let i: number = 0; i < count; i++) {
			out += (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
		}

		return out;
	}
}
