
import { IAddressInfo } from './../shared/models/iaddressinfo';
import { ISocialMedia } from './../shared/models/isocialmedia';

export interface IDealer {
	Id: string;
	DealerId: number;
	ContactName: string;
	CountryId: number;
	Name: string;
	Email: string;
	Phone: string;
	Fax: string;
	Address: IAddressInfo;
	Website: string;
	Latitude: string;
	Longitude: string;
	SocialMedia: ISocialMedia;
}
