
import { ICountry } from './../../country/icountry';

export interface IDistributorLandingProp {
	allCountries: ICountry[];
}