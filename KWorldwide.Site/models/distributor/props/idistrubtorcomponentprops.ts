
import { IDistributor } from './../../distributor/idistributor';
import { IDealer } from './../../distributor/idealer';

export interface IDistributorComponentProp {
	distributorDealerModel: IDistributorDealer;
}

export interface IDistributorDealer {
	allDistributors: IDistributor[];
	allDealers: IDealer[];
}