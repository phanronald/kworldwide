
export interface IInnovations {
	Innovation: IInnovationData[];
}

export interface IInnovationData {
	SlideNumber: number;
	Description: string;
	Title: string;
}