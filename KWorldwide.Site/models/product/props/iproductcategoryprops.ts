
import { IProductSubCategory } from './../iproductsubcategory';
import { IProduct } from './../iproduct';

export interface IProductCategoryProps {
	subCategoriesToShow: IProductSubCategory[];
	productsToShow: IProduct[];
}