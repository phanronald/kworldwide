
import { IProductCategory } from './../iproductcategory';

export interface IProductCategoryIndexProps {
	allCategories: IProductCategory[];
}