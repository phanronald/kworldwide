
import { IProductCategory } from './../product/iproductcategory';

export interface IHeader {
	allCategories: IProductCategory[];
}