
export interface IAddressInfo {
	Address1: string;
	Address2: string;
	City: string;
	StateProvince: string;
	PostalCode: string;
}