
import { IMaps } from './imaps';

export interface IBingMaps extends IMaps {
	title:string;
	subtitle:string;
}