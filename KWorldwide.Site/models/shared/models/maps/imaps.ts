
import { ILatLng } from './ilatlng';

export interface IMaps {
	apiKey:string;
	latLng: ILatLng;
}