
export interface ISocialMedia {
	Facebook: string;
	FacebookId: string;
	Twitter: string;
	TwitterId: string;
	Youtube: string;
	YoutubeAuthor: string;
	Instagram: string;
	InstagramId: string;
}