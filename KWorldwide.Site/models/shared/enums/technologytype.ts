export enum TechnologyType {
    None = 0,
    Engine = 1,
    Chassis = 2
}