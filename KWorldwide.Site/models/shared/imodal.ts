export interface IModal {
	open: boolean;
	children?: React.ReactNode;
	className?: string;
}