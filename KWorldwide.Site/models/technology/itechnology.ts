
import { IMedia } from './imedia';
import { IProductAvailable } from './iproductavaiable';

export interface ITechnology {

	Id: string;
	Name: string;
	TechType: number;
	Image: string;
	Description: string;
	Media: IMedia[];
	ProductAvailable: IProductAvailable[];
	TruncatedDescription: string;
}