
import { ITechnology } from './../itechnology';

export interface ITechnologyProps {
	technologiesToShow: ITechnology[];
}