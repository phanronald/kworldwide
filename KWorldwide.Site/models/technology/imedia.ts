export interface IMedia {
	Image: string;
	Video: string;
	Type: number;
}