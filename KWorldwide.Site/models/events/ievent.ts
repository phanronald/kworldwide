
import { EventType } from './../shared/enums/eventtype';
import { EventCategoryType } from './../shared/enums/eventcategorytype';

export interface IEvent {
	Id: string;
	Image: string;
	Title: string;
	Description: string;
	Link: string;
	EventType: EventType;
	EventCategoryType: EventCategoryType;
	CountryId: number[];
}