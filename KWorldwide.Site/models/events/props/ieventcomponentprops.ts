
import { IEvent } from './../ievent';
import { ICountry } from './../../country/icountry';

export interface IEventComponentProps {
	eventsToShow: IEvent[];
	countriesToShow: ICountry[];
}