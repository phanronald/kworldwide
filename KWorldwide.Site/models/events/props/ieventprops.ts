
import { IEvent } from './../ievent';
import { ICountry } from './../../country/icountry';

export interface IEventProps {
	eventsToShow: IEvent[];
	countriesToShow: ICountry[];
}