
import { EventType } from './../../shared/enums/eventtype';
import { EventCategoryType } from './../../shared/enums/eventcategorytype';

export interface IEventMiniHeaderProps {
	currentEventType: EventType;
	currentEventCategoryType: EventCategoryType;
}