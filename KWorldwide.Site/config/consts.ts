export class Consts {

	public static API_URL: string ='http://localhost:8000';
	public static LOCAL_FILE_URL: string ='http://localhost:3000';

	/* LOCAL STORAGE KEY */
	public static COUNTRY_KEY: string = "klaCountries";
	public static CATEGORY_KEY: string = "klaCategory";


	public static EXPIRATION_OF_ONE_WEEK: number = 10080;
}