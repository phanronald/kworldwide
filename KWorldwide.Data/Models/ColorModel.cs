﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models
{
	public class ColorModel
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("ColorId")]
		public int ColorId { get; set; }

		[BsonElement("ColorName")]
		public string ColorName { get; set; }

		[BsonElement("ImageUrl")]
		public string ImageUrl { get; set; }
	}
}
