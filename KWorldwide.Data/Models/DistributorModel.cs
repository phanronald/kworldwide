﻿using KWorldwide.Data.Models.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models
{
	public class DistributorModel : ContactInfo
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("DistributorId")]
		public int DistributorId { get; set; }

		[BsonElement("DistributorSite")]
		public string DistributorSite { get; set; }

		[BsonElement("MapImage")]
		public string MapImage { get; set; }
	}
}
