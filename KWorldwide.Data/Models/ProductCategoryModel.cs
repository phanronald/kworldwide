﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models
{
	public class ProductCategoryModel
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("ProductCategoryId")]
		public int ProductCategoryId { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("SortOrder")]
		public int SortOrder { get; set; }

		[BsonElement("NavigationUrl")]
		public string NavigationUrl { get; set; }

		[BsonElement("ImageUrl")]
		public string ImageUrl { get; set; }
	}
}
