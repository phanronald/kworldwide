﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KWorldwide.Data.Models
{
	public class EventModel
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("Title")]
		public string Title { get; set; }

		[BsonElement("Image")]
		public string Image { get; set; }

		[BsonElement("Description")]
		public string Description { get; set; }

		[BsonElement("Link")]
		public string Link { get; set; }

		[BsonElement("EventType")]
		public int EventType { get; set; }

		[BsonElement("CategoryType")]
		public int CategoryType { get; set; }

		[BsonElement("CountryId")]
		public int[] CountryId { get; set; }
	}
}
