﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models.Shared
{
	public class AddressInfo
	{
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string StateProvince { get; set; }
		public string PostalCode { get; set; }
	}
}
