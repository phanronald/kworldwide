﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models.Shared
{
	public abstract class ContactInfo
	{
		[BsonElement("CountryId")]
		public int CountryId { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("Email")]
		public string Email { get; set; }

		[BsonElement("Phone")]
		public string Phone { get; set; }

		[BsonElement("Fax")]
		public string Fax { get; set; }

		[BsonElement("Address")]
		public AddressInfo Address { get; set; }

		[BsonElement("Website")]
		public string Website { get; set; }

		[BsonElement("Latitude")]
		public string Latitude { get; set; }

		[BsonElement("Longitude")]
		public string Longitude { get; set; }

		[BsonElement("SocialMedia")]
		public SocialMedia SocialMedia { get; set; }
	}
}
