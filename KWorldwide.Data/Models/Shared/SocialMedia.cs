﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models.Shared
{
	public class SocialMedia
	{
		public string Facebook { get; set; }
		public string FacebookId { get; set; }
		public string Twitter { get; set; }
		public string TwitterId { get; set; }
		public string Youtube { get; set; }
		public string YoutubeAuthor { get; set; }
		public string Instagram { get; set; }
		public string InstagramId { get; set; }
	}
}
