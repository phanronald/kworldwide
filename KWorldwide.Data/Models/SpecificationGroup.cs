﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models
{
	public class SpecificationGroup
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("SpecGroupId")]
		public int SpecGroupId { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("SortOrder")]
		public int SortOrder { get; set; }
	}
}
