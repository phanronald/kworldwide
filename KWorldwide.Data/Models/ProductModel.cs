﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models
{
	public class ProductModel
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("ProductId")]
		public int ProductId { get; set; }

		[BsonElement("CategoryId")]
		public int CategoryId { get; set; }

		[BsonElement("SubCategoryId")]
		public int SubCategoryId { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("MSRP")]
		public float MSRP { get; set; }

		[BsonElement("Year")]
		public int Year { get; set; }

		[BsonElement("UrlKey")]
		public string UrlKey { get; set; }

		[BsonElement("IsActive")]
		public bool IsActive { get; set; }

		[BsonElement("IsNavigation")]
		public bool IsNavigation { get; set; }

		[BsonElement("NavImageUrl")]
		public string NavImageUrl { get; set; }

		[BsonElement("SortOrder")]
		public int SortOrder { get; set; }
	}
}
