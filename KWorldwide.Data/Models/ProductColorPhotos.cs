﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models
{
	public class ProductColorPhotos
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("ProductColorPhotoId")]
		public int ProductColorPhotoId { get; set; }

		[BsonElement("ProductId")]
		public int ProductId { get; set; }

		[BsonElement("ColorId")]
		public int ColorId { get; set; }

		[BsonElement("ImageUrl")]
		public string ImageUrl { get; set; }
	}
}
