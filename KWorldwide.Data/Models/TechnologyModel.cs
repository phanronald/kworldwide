﻿using KWorldwide.Data.Models.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models
{
	public class TechnologyModel
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("TechType")]
		public int TechType { get; set; }

		[BsonElement("Image")]
		public string Image { get; set; }

		[BsonElement("Description")]
		public string Description { get; set; }

		[BsonElement("Media")]
		public Media[] Media { get; set; }

		[BsonElement("ProductAvailable")]
		public ProductAvailable[] ProductAvailable { get; set; }
	}
}
