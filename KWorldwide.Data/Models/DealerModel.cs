﻿using KWorldwide.Data.Models.Shared;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models
{
	public class DealerModel : ContactInfo
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("DealerId")]
		public int DealerId { get; set; }

		[BsonElement("ContactName")]
		public string ContactName { get; set; }
	}
}
