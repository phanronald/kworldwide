﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models
{
	public class CountryModel
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("CountryId")]
		public int CountryId { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("UrlKey")]
		public string UrlKey { get; set; }
	}
}
