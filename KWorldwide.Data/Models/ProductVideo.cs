﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models
{
	public class ProductVideo
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("ProductId")]
		public int ProductId { get; set; }

		[BsonElement("FileName")]
		public string FileName { get; set; }

		[BsonElement("ThumbnailName")]
		public string ThumbnailName { get; set; }

		[BsonElement("Title")]
		public string Title { get; set; }
	}
}
