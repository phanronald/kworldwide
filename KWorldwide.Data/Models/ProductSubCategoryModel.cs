﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models
{
	public class ProductSubCategoryModel
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("ProductSubCategoryId")]
		public int ProductSubCategoryId { get; set; }

		[BsonElement("ProductCategoryId")]
		public int ProductCategoryId { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("IsActive")]
		public bool IsActive { get; set; }

		[BsonElement("SortOrder")]
		public int SortOrder { get; set; }
	}
}
