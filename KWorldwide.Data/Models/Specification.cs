﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Data.Models
{
	public class Specification
	{
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string Id { get; set; }

		[BsonElement("ProductSpecId")]
		public int ProductSpecId { get; set; }

		[BsonElement("SpecGroupId")]
		public int SpecGroupId { get; set; }

		[BsonElement("Name")]
		public string Name { get; set; }

		[BsonElement("SortOrder")]
		public int SortOrder { get; set; }

		[BsonElement("IsDisplayed")]
		public bool IsDisplayed { get; set; }
	}
}
