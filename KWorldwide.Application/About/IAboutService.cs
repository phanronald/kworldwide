﻿using KWorldwide.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Application.About
{
	public interface IAboutService
	{
		Task<AboutModel> GetAllAbout();
	}
}
