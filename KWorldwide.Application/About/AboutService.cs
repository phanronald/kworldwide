﻿using AutoMapper;
using KWorldwide.Business.About;
using KWorldwide.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Application.About
{
	public class AboutService : IAboutService
	{
		private readonly IMapper _mapper;
		private readonly IAboutRepo _aboutRepo;

		public AboutService(IMapper mapper, IAboutRepo aboutRepo)
		{
			_mapper = mapper;
			_aboutRepo = aboutRepo;
		}

		public async Task<AboutModel> GetAllAbout()
		{
			var aboutData = await _aboutRepo.GetAboutData();
			return _mapper.Map<AboutModel>(aboutData);
		}
	}
}
