﻿using KWorldwide.Application.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.About.Models
{
	public class AboutModel
	{
		public string Id { get; set; }
		public string HeaderText { get; set; }
		public string HeaderImage { get; set; }
		public string Overview { get; set; }
		public string Detailed { get; set; }
		public BasicContact Contact { get; set; }
		public string MainAddress { get; set; }
		public string MailAddress { get; set; }
		public string Phone { get; set; }
		public Link[] AboutLinks { get; set; }
	}
}
