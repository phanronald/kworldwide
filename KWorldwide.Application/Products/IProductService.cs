﻿using KWorldwide.Application.Products.Models;
using KWorldwide.Application.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Application.Products
{
	public interface IProductService
	{
		Task<List<ProductCategoryModel>> GetProductCategories();
		Task<List<ProductColorPhotos>> GetAllProductColorPhotos();
		Task<List<ProductSubCategoryModel>> GetProductSubCategories();

		Task<List<ProductModel>> GetProducts();
		Task<List<ProductPhoto>> GetAllProductPhotos();
		Task<List<ProductVideo>> GetAllProductVideos();

		Task<ProductCategoryModel> Get_Category_ById(int categoryId);
		Task<ProductSubCategoryModel> Get_SubCategory_ById(int subCategoryId);

		Task<ProductModel> Get_Product_ById(int productId);
		Task<List<ProductPhoto>> GetProductPhotos_ByProductId(int productId);
		Task<List<ProductVideo>> GetProductVideos_ByProductId(int productId);
		Task<List<ProductColorPhotos>> GetProductColorPhotos_ById(int productId);
	}
}
