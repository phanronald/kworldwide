﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Products.Models
{
	public class ProductColorPhotos
	{
		public string Id { get; set; }
		public int ProductColorPhotoId { get; set; }
		public int ProductId { get; set; }
		public int ColorId { get; set; }
		public string ImageUrl { get; set; }
	}
}
