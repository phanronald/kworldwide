﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Products.Models
{
	public class ProductSubCategoryModel
	{
		public string Id { get; set; }
		public int ProductSubCategoryId { get; set; }
		public int ProductCategoryId { get; set; }
		public string Name { get; set; }
		public bool IsActive { get; set; }
		public int SortOrder { get; set; }
	}
}
