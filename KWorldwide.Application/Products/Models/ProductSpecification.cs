﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Products.Models
{
	public class ProductSpecification
	{
		public string Id { get; set; }
		public int ProductSpecId { get; set; }
		public int ProductId { get; set; }
		public int SpecId { get; set; }
		public string SpecValue { get; set; }
		public bool IsFeatured { get; set; }
	}
}
