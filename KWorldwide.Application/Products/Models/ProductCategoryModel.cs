﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Products.Models
{
	public class ProductCategoryModel
	{
		public string Id { get; set; }
		public int ProductCategoryId { get; set; }
		public string Name { get; set; }
		public int SortOrder { get; set; }
		public string NavigationUrl { get; set; }
		public string ImageUrl { get; set; }
	}
}
