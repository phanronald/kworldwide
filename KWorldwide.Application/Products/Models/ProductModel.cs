﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Products.Models
{
	public class ProductModel
	{
		public string Id { get; set; }
		public int ProductId { get; set; }
		public int CategoryId { get; set; }
		public int SubCategoryId { get; set; }
		public string Name { get; set; }
		public float MSRP { get; set; }
		public int Year { get; set; }
		public string UrlKey { get; set; }
		public bool IsActive { get; set; }
		public bool IsNavigation { get; set; }
		public string NavImageUrl { get; set; }
		public int SortOrder { get; set; }
	}
}
