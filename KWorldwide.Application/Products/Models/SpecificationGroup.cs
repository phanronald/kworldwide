﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Products.Models
{
	public class SpecificationGroup
	{
		public string Id { get; set; }
		public int SpecGroupId { get; set; }
		public string Name { get; set; }
		public int SortOrder { get; set; }
	}
}
