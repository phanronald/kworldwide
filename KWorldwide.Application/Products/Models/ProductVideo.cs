﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Products.Models
{
	public class ProductVideo
	{
		public string Id { get; set; }
		public int ProductId { get; set; }
		public string FileName { get; set; }
		public string ThumbnailName { get; set; }
		public string Title { get; set; }
	}
}
