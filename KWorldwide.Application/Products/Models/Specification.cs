﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Products.Models
{
	public class Specification
	{
		public string Id { get; set; }
		public int ProductSpecId { get; set; }
		public int SpecGroupId { get; set; }
		public string Name { get; set; }
		public int SortOrder { get; set; }
		public bool IsDisplayed { get; set; }
	}
}
