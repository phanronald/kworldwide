﻿using AutoMapper;
using KWorldwide.Application.Products.Models;
using KWorldwide.Business.Products;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace KWorldwide.Application.Products
{
	public class ProductService : IProductService
	{
		private readonly IMapper _mapper;
		private readonly IProductRepo _productRepo;

		public ProductService(IMapper mapper, IProductRepo productRepo)
		{
			_mapper = mapper;
			_productRepo = productRepo;
		}

		public async Task<List<ProductColorPhotos>> GetAllProductColorPhotos()
		{
			var allProductColorPhotos = await _productRepo.GetAllProductColorPhotos();
			return _mapper.Map<List<ProductColorPhotos>>(allProductColorPhotos);
		}

		public async Task<List<ProductPhoto>> GetAllProductPhotos()
		{
			var allProductPhotos = await _productRepo.GetAllProductPhotos();
			return _mapper.Map<List<ProductPhoto>>(allProductPhotos);
		}

		public async Task<List<ProductVideo>> GetAllProductVideos()
		{
			var allProductVideos = await _productRepo.GetAllProductVideos();
			return _mapper.Map<List<ProductVideo>>(allProductVideos);
		}

		public async Task<List<ProductCategoryModel>> GetProductCategories()
		{
			var allProductCategories = await _productRepo.GetProductCategories();
			return _mapper.Map<List<ProductCategoryModel>>(allProductCategories);
		}

		public async Task<List<ProductColorPhotos>> GetProductColorPhotos_ById(int productId)
		{
			var listOfProductColorPhotos = await GetAllProductColorPhotos();
			if(listOfProductColorPhotos.Any())
			{
				return listOfProductColorPhotos.Where(x => x.ProductId == productId).ToList();
			}

			return new List<ProductColorPhotos>();
		}

		public async Task<List<ProductPhoto>> GetProductPhotos_ByProductId(int productId)
		{
			var allProductPhotos = await _productRepo.GetAllProductPhotos();
			if(allProductPhotos.Any())
			{
				var foundProductPhotos = allProductPhotos.Where(x => x.ProductId == productId).ToList();
				if(foundProductPhotos.Any())
				{
					return _mapper.Map<List<ProductPhoto>>(foundProductPhotos);
				}
			}

			return new List<ProductPhoto>();
		}

		public async Task<List<ProductModel>> GetProducts()
		{
			var allProducts = await _productRepo.GetProducts();
			return _mapper.Map<List<ProductModel>>(allProducts);
		}

		public async Task<List<ProductSubCategoryModel>> GetProductSubCategories()
		{
			var allProductSubCatgeories = await _productRepo.GetProductSubCategories();
			return _mapper.Map<List<ProductSubCategoryModel>>(allProductSubCatgeories);
		}

		public async Task<List<ProductVideo>> GetProductVideos_ByProductId(int productId)
		{
			var allProductVideos = await _productRepo.GetAllProductVideos();
			if (allProductVideos.Any())
			{
				var foundProductPhotos = allProductVideos.Where(x => x.ProductId == productId).ToList();
				if (foundProductPhotos.Any())
				{
					return _mapper.Map<List<ProductVideo>>(foundProductPhotos);
				}
			}

			return new List<ProductVideo>();
		}

		public async Task<ProductCategoryModel> Get_Category_ById(int categoryId)
		{
			var listOfCategories = await GetProductCategories();
			if (listOfCategories.Any())
			{
				var foundCategory = listOfCategories.Where(x => x.ProductCategoryId == categoryId);
				if (foundCategory.Any())
				{
					return foundCategory.FirstOrDefault();
				}
			}

			return null;
		}

		public async Task<ProductModel> Get_Product_ById(int productId)
		{
			var listOfProducts = await GetProducts();
			if (listOfProducts.Any())
			{
				var foundProducts = listOfProducts.Where(x => x.ProductId == productId);
				if (foundProducts.Any())
				{
					return foundProducts.FirstOrDefault();
				}
			}

			return null;
		}

		public async Task<ProductSubCategoryModel> Get_SubCategory_ById(int subCategoryId)
		{
			var listOfSubCategories = await GetProductSubCategories();
			if (listOfSubCategories.Any())
			{
				var foundSubCategory = listOfSubCategories.Where(x => x.ProductSubCategoryId == subCategoryId);
				if (foundSubCategory.Any())
				{
					return foundSubCategory.FirstOrDefault();
				}
			}

			return null;
		}
	}
}
