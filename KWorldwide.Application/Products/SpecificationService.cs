﻿using AutoMapper;
using KWorldwide.Application.Products.Models;
using KWorldwide.Business.Products;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KWorldwide.Application.Products
{
	public class SpecificationService : ISpecificationService
	{
		private readonly IMapper _mapper;
		private readonly ISpecificationRepo _specificationRepo;

		public SpecificationService(IMapper mapper, ISpecificationRepo specificationRepo)
		{
			_mapper = mapper;
			_specificationRepo = specificationRepo;
		}

		public async Task<List<ProductSpecification>> GetAllProductSpecification()
		{
			var allProductSpecs = await _specificationRepo.GetAllProductSpecification();
			return _mapper.Map<List<ProductSpecification>>(allProductSpecs);
		}

		public async Task<List<ProductSpecification>> GetAllProductSpecifications_ByProductId(int productId)
		{
			var allProductSpecs = await GetAllProductSpecification();
			if (allProductSpecs != null)
			{
				return allProductSpecs.Where(x => x.ProductId == productId).ToList();
			}

			return new List<ProductSpecification>();
		}

		public async Task<List<SpecificationGroup>> GetAllSpecGroups_ById(int specGroupId)
		{
			var allSpecGroups = await GetAllSpecificationGroups();
			if (allSpecGroups != null)
			{
				return allSpecGroups.Where(x => x.SpecGroupId == specGroupId).ToList();
			}

			return new List<SpecificationGroup>();
		}

		public async Task<List<SpecificationGroup>> GetAllSpecificationGroups()
		{
			var allSpecGroups = await _specificationRepo.GetAllSpecificationGroups();
			return _mapper.Map<List<SpecificationGroup>>(allSpecGroups);
		}

		public async Task<List<Specification>> GetAllSpecifications()
		{
			var allSpecs = await _specificationRepo.GetAllSpecifications();
			return _mapper.Map<List<Specification>>(allSpecs);
		}

		public async Task<List<Specification>> GetAllSpecifications_ById(int specId)
		{
			var allSpecifications = await GetAllSpecifications();
			if (allSpecifications != null)
			{
				return allSpecifications.Where(x => x.ProductSpecId == specId).ToList();
			}

			return new List<Specification>();
		}
	}
}
