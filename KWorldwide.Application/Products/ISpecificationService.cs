﻿using KWorldwide.Application.Products.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KWorldwide.Application.Products
{
	public interface ISpecificationService
	{
		Task<List<SpecificationGroup>> GetAllSpecificationGroups();
		Task<List<Specification>> GetAllSpecifications();
		Task<List<ProductSpecification>> GetAllProductSpecification();
		Task<List<ProductSpecification>> GetAllProductSpecifications_ByProductId(int productId);
		Task<List<SpecificationGroup>> GetAllSpecGroups_ById(int specGroupId);
		Task<List<Specification>> GetAllSpecifications_ById(int specId);
	}
}
