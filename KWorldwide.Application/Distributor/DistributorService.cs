﻿using AutoMapper;
using KWorldwide.Application.Distributor.Models;
using KWorldwide.Business.Distributor;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Application.Distributor
{
	public class DistributorService : IDistributorService
	{
		private readonly IMapper _mapper;
		private readonly IDistributorRepo _distributorRepo;

		public DistributorService(IMapper mapper, IDistributorRepo distributorRepo)
		{
			_mapper = mapper;
			_distributorRepo = distributorRepo;
		}

		public async Task<List<DistributorModel>> GetAllDistributors()
		{
			var allDistributors = await _distributorRepo.GetAllDistributors();
			return _mapper.Map<List<DistributorModel>>(allDistributors);
		}

		public async Task<DistributorModel> GetDistributorById(int distributorId)
		{
			var distributor = await _distributorRepo.GetDistributorById(distributorId);
			return _mapper.Map<DistributorModel>(distributor);
		}
	}
}
