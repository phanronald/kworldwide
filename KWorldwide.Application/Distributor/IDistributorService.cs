﻿using KWorldwide.Application.Distributor.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Application.Distributor
{
	public interface IDistributorService
	{
		Task<List<DistributorModel>> GetAllDistributors();
		Task<DistributorModel> GetDistributorById(int distributorId);
	}
}
