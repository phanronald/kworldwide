﻿using KWorldwide.Application.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Distributor.Models
{
	public class DistributorModel
	{
		public string Id { get; set; }
		public int DistributorId { get; set; }
		public string DistributorSite { get; set; }
		public string MapImage { get; set; }

		public int CountryId { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public string Fax { get; set; }
		public AddressInfo Address { get; set; }
		public string Website { get; set; }
		public string Latitude { get; set; }
		public string Longitude { get; set; }
		public SocialMedia SocialMedia { get; set; }
	}
}
