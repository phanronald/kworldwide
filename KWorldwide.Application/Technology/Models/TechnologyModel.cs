﻿using KWorldwide.Application.Shared;

namespace KWorldwide.Application.Technology.Models
{
	public class TechnologyModel
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public int TechType { get; set; }
		public string Image { get; set; }
		public string Description { get; set; }
		public Media[] Media { get; set; }
		public ProductAvailable[] ProductAvailable { get; set; }
	}
}
