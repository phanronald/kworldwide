﻿using AutoMapper;
using KWorldwide.Application.Technology.Models;
using KWorldwide.Business.Technology;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Application.Technology
{
	public class TechnologyService : ITechnologyService
	{
		private readonly IMapper _mapper;
		private readonly ITechnologyRepo _technologyRepo;

		public TechnologyService(IMapper mapper, ITechnologyRepo technologyRepo)
		{
			_mapper = mapper;
			_technologyRepo = technologyRepo;
		}

		public async Task<List<TechnologyModel>> GetTechnologies()
		{
			var allTechnologies = await _technologyRepo.GetTechnologies();
			return _mapper.Map<List<TechnologyModel>>(allTechnologies);
		}
	}
}
