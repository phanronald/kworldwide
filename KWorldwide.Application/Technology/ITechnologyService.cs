﻿using KWorldwide.Application.Technology.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KWorldwide.Application.Technology
{
	public interface ITechnologyService
	{
		Task<List<TechnologyModel>> GetTechnologies();
	}
}
