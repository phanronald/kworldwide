﻿using KWorldwide.Application.Events.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Application.Events
{
	public interface IEventService
	{
		Task<List<EventModel>> GetAllEvents();
		Task<List<EventModel>> GetEventsByEventType(int eventType);
		Task<List<EventModel>> GetEventsByEventCategoryType(int eventType, 
													int eventCategoryType);
	}
}
