﻿using KWorldwide.Common.Enums;

namespace KWorldwide.Application.Events.Models
{
	public class EventModel
	{
		public string Id { get; set; }

		public string Title { get; set; }

		public string Image { get; set; }

		public string Description { get; set; }

		public string Link { get; set; }

		public EventType EventType { get; set; }

		public EventCategoryType CategoryType { get; set; }

		public int[] CountryId { get; set; }
	}
}
