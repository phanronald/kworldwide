﻿using AutoMapper;
using KWorldwide.Application.Events.Models;
using KWorldwide.Business.Events;
using KWorldwide.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Application.Events
{
	public class EventService : IEventService
	{
		private readonly IMapper _mapper;
		private readonly IEventRepo _eventRepo;

		public EventService(IMapper mapper, IEventRepo eventRepo)
		{
			_mapper = mapper;
			_eventRepo = eventRepo;
		}

		public async Task<List<EventModel>> GetAllEvents()
		{
			var allEvents = await _eventRepo.GetAllEvents();
			return _mapper.Map<List<EventModel>>(allEvents);
		}

		public async Task<List<EventModel>> GetEventsByEventCategoryType(int eventType, int eventCategoryType)
		{
			var allEvents = await GetAllEvents();
			var eventsByCategoryType = allEvents.Where(x => x.EventType == (EventType)eventType &&
				x.CategoryType == (EventCategoryType)eventCategoryType);

			return eventsByCategoryType.ToList();
		}

		public async Task<List<EventModel>> GetEventsByEventType(int eventType)
		{
			var allEvents = await GetAllEvents();
			var eventsByType = allEvents.Where(x => x.EventType == 
				(EventType)eventType);

			return eventsByType.ToList();
		}
	}
}
