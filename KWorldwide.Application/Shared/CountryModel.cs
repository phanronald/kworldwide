﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Shared
{
	public class CountryModel
	{
		public string Id { get; set; }
		public int CountryId { get; set; }
		public string Name { get; set; }
		public string UrlKey { get; set; }
	}
}
