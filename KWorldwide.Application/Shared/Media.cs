﻿using KWorldwide.Common.Enums;

namespace KWorldwide.Application.Shared
{
	public class Media
	{
		public string Image { get; set; }
		public string Video { get; set; }
		public MediaType MediaType { get; set; }
	}
}
