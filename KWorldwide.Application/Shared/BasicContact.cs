﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Shared
{
	public class BasicContact
	{
		public string Text { get; set; }
		public string Email { get; set; }
	}
}
