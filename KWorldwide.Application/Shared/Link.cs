﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Shared
{
	public class Link
	{
		public string Text { get; set; }
		public string Url { get; set; }
	}
}
