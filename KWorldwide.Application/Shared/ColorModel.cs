﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Shared
{
	public class ColorModel
	{
		public string Id { get; set; }
		public int ColorId { get; set; }
		public string ColorName { get; set; }
		public string ImageUrl { get; set; }
	}
}
