﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KWorldwide.Application.Shared
{
	public class ProductAvailable
	{
		public string Name { get; set; }
		public string Link { get; set; }
	}
}
