﻿using AutoMapper;
using KWorldwide.Application.Dealer.Model;
using KWorldwide.Business.Dealer;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Application.Dealer
{
	public class DealerService : IDealerService
	{
		private readonly IMapper _mapper;
		private readonly IDealerRepo _dealerRepo;

		public DealerService(IMapper mapper, IDealerRepo dealerRepo)
		{
			_mapper = mapper;
			_dealerRepo = dealerRepo;
		}

		public async Task<List<DealerModel>> GetAllDealers()
		{
			var allDealers = await _dealerRepo.GetAllDealers();
			return _mapper.Map<List<DealerModel>>(allDealers);
		}
	}
}
