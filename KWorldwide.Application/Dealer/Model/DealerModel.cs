﻿using KWorldwide.Application.Shared;

namespace KWorldwide.Application.Dealer.Model
{
	public class DealerModel
	{
		public string Id { get; set; }
		public int DealerId { get; set; }
		public string ContactName { get; set; }

		public int CountryId { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public string Fax { get; set; }
		public AddressInfo Address { get; set; }
		public string Website { get; set; }
		public string Latitude { get; set; }
		public string Longitude { get; set; }
		public SocialMedia SocialMedia { get; set; }
	}
}
