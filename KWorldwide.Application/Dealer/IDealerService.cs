﻿using KWorldwide.Application.Dealer.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Application.Dealer
{
	public interface IDealerService
	{
		Task<List<DealerModel>> GetAllDealers();
	}
}
