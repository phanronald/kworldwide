﻿using AutoMapper;
using KWorldwide.Common.Enums;
using KAS = KWorldwide.Application.Shared;
using KDM = KWorldwide.Data.Models;
using KDMS = KWorldwide.Data.Models.Shared;

namespace KWorldwide.Application.AutomapperProfile
{
	public class SharedAutoMapperProfile : Profile
	{
		public SharedAutoMapperProfile()
		{
			CreateMap<KDM.CountryModel, KAS.CountryModel>();
			CreateMap<KDMS.BasicContact, KAS.BasicContact>();
			CreateMap<KDMS.Link, KAS.Link>();
			CreateMap<KDMS.ProductAvailable, KAS.ProductAvailable>();
			CreateMap<KDM.ColorModel, KAS.ColorModel>();
			CreateMap<KDMS.AddressInfo, KAS.AddressInfo>();
			CreateMap<KDMS.SocialMedia, KAS.SocialMedia>();

			CreateMap<KDMS.Media, KAS.Media>()
				.ForMember(dest => dest.MediaType, opts => opts.MapFrom(src => (MediaType)src.Type));
		}
	}
}
