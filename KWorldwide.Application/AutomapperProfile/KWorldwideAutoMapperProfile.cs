﻿using AutoMapper;
using KDMS = KWorldwide.Data.Models;

namespace KWorldwide.Application.AutomapperProfile
{
	public class KWorldwideAutoMapperProfile : Profile
	{
		public KWorldwideAutoMapperProfile()
		{
			CreateMap<KDMS.AboutModel, About.Models.AboutModel>();
			CreateMap<KDMS.TechnologyModel, Technology.Models.TechnologyModel>();
			CreateMap<KDMS.EventModel, Events.Models.EventModel>();
			CreateMap<KDMS.DistributorModel, Distributor.Models.DistributorModel>();
			CreateMap<KDMS.DealerModel, Dealer.Model.DealerModel>();

			CreateMap<KDMS.ProductCategoryModel, Products.Models.ProductCategoryModel>();
			CreateMap<KDMS.ProductColorPhotos, Products.Models.ProductColorPhotos>();
			CreateMap<KDMS.ProductModel, Products.Models.ProductModel>();
			CreateMap<KDMS.ProductPhoto, Products.Models.ProductPhoto>();
			CreateMap<KDMS.ProductSpecification, Products.Models.ProductSpecification>();
			CreateMap<KDMS.ProductSubCategoryModel, Products.Models.ProductSubCategoryModel>();
			CreateMap<KDMS.ProductVideo, Products.Models.ProductVideo>();
			CreateMap<KDMS.Specification, Products.Models.Specification>();
			CreateMap<KDMS.SpecificationGroup, Products.Models.SpecificationGroup>();
		}
	}
}
