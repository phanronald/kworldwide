﻿using KWorldwide.Application.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Application.Color
{
	public interface IColorService
	{
		Task<List<ColorModel>> GetAllColors();
		Task<ColorModel> GetColor_ByColorId(int colorId);
	}
}
