﻿using AutoMapper;
using KWorldwide.Application.Shared;
using KWorldwide.Business.Color;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KWorldwide.Application.Color
{
	public class ColorService : IColorService
	{
		private readonly IMapper _mapper;
		private readonly IColorRepo _colorRepo;

		public ColorService(IMapper mapper, IColorRepo colorRepo)
		{
			_mapper = mapper;
			_colorRepo = colorRepo;
		}

		public async Task<List<ColorModel>> GetAllColors()
		{
			var allColors = await _colorRepo.GetAllColors();
			return _mapper.Map<List<ColorModel>>(allColors);
		}

		public async Task<ColorModel> GetColor_ByColorId(int colorId)
		{
			var listOfColors = await GetAllColors();
			if (listOfColors.Any())
			{
				var foundColors = listOfColors.Where(x => x.ColorId == colorId);
				if (foundColors.Any())
				{
					return foundColors.FirstOrDefault();
				}
			}

			return null;
		}
	}
}
