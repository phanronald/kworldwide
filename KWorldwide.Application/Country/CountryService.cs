﻿using AutoMapper;
using KWorldwide.Application.Shared;
using KWorldwide.Business.Country;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KWorldwide.Application.Country
{
	public class CountryService : ICountryService
	{
		private readonly IMapper _mapper;
		private readonly ICountryRepo _countryRepo;

		public CountryService(IMapper mapper, ICountryRepo countryRepo)
		{
			_mapper = mapper;
			_countryRepo = countryRepo;
		}

		public async Task<List<CountryModel>> GetAllCountries()
		{
			var allCountries = await _countryRepo.GetAllCountries();
			return _mapper.Map<List<CountryModel>>(allCountries);
		}
	}
}
