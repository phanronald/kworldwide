﻿using KWorldwide.Application.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KWorldwide.Application.Country
{
	public interface ICountryService
	{
		Task<List<CountryModel>> GetAllCountries();
	}
}
